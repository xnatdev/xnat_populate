import org.nrg.populate.DataManager
import org.nrg.populate.XnatDriver
import org.nrg.populate.tcia.TciaProcessor
import org.nrg.populate.util.ConsoleUtils
import org.nrg.populate.util.Globals
import org.nrg.populate.util.YamlUtils
import org.nrg.populate.xnat_objects.ContainerRegistry
import org.nrg.populate.xnat_objects.SiteConfiguration

@GrabResolver(name='NRG Release Repo', root='https://nrgxnat.jfrog.io/nrgxnat/libs-release')
@GrabResolver(name='NRG Snapshot Repo', root='https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot')
@Grapes([
        @GrabExclude("org.codehaus.groovy:groovy-all"),
        @GrabExclude("org.codehaus.groovy:groovy-xml"),
        @GrabExclude("org.codehaus.groovy:groovy-json"),
        @Grab(group = 'commons-net', module = 'commons-net', version = '3.5'),
        @Grab(group = 'commons-beanutils', module = 'commons-beanutils', version = '1.9.3'),
        @Grab(group = 'org.reflections', module = 'reflections', version = '0.9.10'),
        @Grab(group = 'org.apache.ant', module = 'ant', version = '1.9.4'),
        @Grab(group = 'org.nrg', module = 'grxnat', version = '1.15')
])

final CliBuilder cli = new CliBuilder(
        usage: 'groovy PopulateXnat.groovy -u adminUsername -p adminPassword -s siteUrl -d targetData',
        header: '\nParameters:\n',
        footer: 'The data parameter specifies a text file that contains newline separated data to populate XNAT (version 1.7 required).')

cli.with {
    h( longOpt: 'help', 'Usage Information', required: false)
    u( longOpt: 'user', 'Username for admin account', args: 1, required: false)
    p( longOpt: 'pass', 'Password for the user', args: 1, required: false)
    s( longOpt: 'url', 'XNAT URL', args: 1, required: true)
    d( longOpt: 'data', 'Text file containing list of projects to be uploaded (separated by newlines). allData.txt can be used to send all sample data. Alternatively, specify project list here where project IDs are separated by commas.', args: 1, required: false)
    c( longOpt: 'clean', 'Delete projects matching data on start (DANGEROUS)', required: false)
    r( longOpt: 'refresh-data', 'Delete contents of ./data to grab all new data and metadata YAML', required: false)
    y( longOpt: 'refresh-yaml', 'Delete all YAML files cached in ./data and use versions from sample_projects (if available)', required: false)
    g( longOpt: 'config', 'YAML file to specify site config properties', args: 1, required: false)
    k( longOpt: 'insecure', 'Ignore SSL errors for dev/test environments.', required: false)
    e( longOpt: 'export-mode', 'Specify to export projects in data list FROM XNAT instead of adding them TO XNAT.', required: false)
    w( longOpt: 'delay', 'Optional delay (in milliseconds) to wait between session imports', args: 1, required: false)
    m( longOpt: 'prompt', 'Specify this flag to allow prompting for the password to use instead of requiring it to be provided.', required: false)
    t( longOpt: 'tcia-projects', 'Specify a comma-separated string of projects from The Cancer Imaging Archive (TCIA) to be pulled and populated into XNAT', args: 1, required: false)
    su(longOpt: 'secure-users', 'Only add existing users to projects (new users will not be created)', required: false)
    ch(longOpt: 'cache', 'Only downloads the required data instead of populating it', required: false)
}

if ('-h' in args || '--help' in args) {
    cli.usage()
    return
}

final OptionAccessor params = cli.parse(args)
final String config = params.config ?: null
final boolean refreshData = params.r as boolean
final boolean refreshYaml = params.y as boolean
final String user = readUser(params)
final String pass = readPassword(user, params)
final String url = params.s as String
final boolean clear = params.c as boolean
final boolean insecureSSL = params.k as boolean
final boolean secureUsers = params.su as boolean
final boolean exportMode = params.e as boolean
final boolean cacheMode = params.ch as boolean
final List<String> projects = readProjects(params)
Globals.siteUrl = url
Globals.delay = (params.w) ? params.w as int : 0

final XnatDriver xnat = (!cacheMode) ? new XnatDriver(user, pass, url, insecureSSL, secureUsers) : null

if (exportMode) {
    xnat.export(projects)
} else {
    final SiteConfiguration siteConfiguration = (config) ? YamlUtils.deserializeYaml(config, SiteConfiguration.class) : new SiteConfiguration()
    final DataManager dataManager = new DataManager(refreshData, refreshYaml, projects, readTciaProjects(params))
    dataManager.clean()
    dataManager.downloadData()
    final TciaProcessor tciaProcessor = new TciaProcessor()
    tciaProcessor.processProjects()

    if (!cacheMode) {
        xnat.initialize()
        xnat.checkProjectDependencies()
        xnat.checkPlugins()
        siteConfiguration.post()
        ContainerRegistry.pullImages()

        if (clear) xnat.fresh()
        xnat.postData()
    }
}

String readUser(OptionAccessor params) {
    final String provided = params.u ?: null
    if (provided == null) {
        ConsoleUtils.inform('No username provided, falling back to default of "admin".')
        'admin'
    } else {
        provided
    }
}

String readPassword(String user, OptionAccessor params) {
    final String provided = params.p ?: null
    if (provided == null) {
        if (params.m as boolean) {
            System.console().readPassword("Password for XNAT account '${user}': ") as String
        } else {
            ConsoleUtils.inform('No password provided, falling back to default of "admin".')
            'admin'
        }
    } else {
        provided
    }
}

List<String> readProjects(OptionAccessor params) {
    final String data = params.d ?: null
    if (data) {
        final List<String> projects = []
        final File projectListFile = new File(data)
        if (projectListFile.exists()) {
            println("Data file found: ${projectListFile}...")
            projects.addAll(projectListFile.readLines())
            projects.remove(null)
            projects.remove('')
            projects.remove(' ')
        } else {
            println("Data file not found. Interpretting data input as uploading comma-separated list of projects: ${data}...")
            projects.addAll(data.split(','))
        }
        projects
    } else {
        []
    }
}

List<String> readTciaProjects(OptionAccessor params) {
    final String tciaString = params.t ?: null
    (tciaString?.split(',') ?: []) as List<String>
}