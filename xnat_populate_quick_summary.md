# XNAT Populate Quick Summary

If you are new to XNAT Populate, or want more detailed descriptions, you should also read the [Cheat Sheet](xnat_populate_cheat_sheet.md). If you're an XNAT developer looking to see at a glance if a particular configuration (or type of data) is available out-of-the-box, see the tables below:

| I'm looking for DICOM data of a particular modality...                      | Find it in...                                                                     |
|-----------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| AR                                                                          | Synthetic_SOP                                                                     |
| ASMT                                                                        | Synthetic_SOP                                                                     |
| AU                                                                          | R9Q_BW                                                                            |
| CR                                                                          | chestQ3, IHE_MESA_test                                                            |
| CT                                                                          | chestQ3, IHE_MESA_test, PETCT_x9, SEG_DEMO                                        |
| DX                                                                          | chestQ3                                                                           |
| ECG                                                                         | R9Q_BW                                                                            |
| ES                                                                          | Q0L_BW                                                                            |
| FID                                                                         | SlicerRtData                                                                      |
| HC                                                                          | Q0L_BW                                                                            |
| HD                                                                          | Q0L_BW                                                                            |
| IO                                                                          | Q0L_BW                                                                            |
| IOL                                                                         | Synthetic_SOP                                                                     |
| KER                                                                         | Synthetic_SOP                                                                     |
| KO                                                                          | QIN_Breast_DCE-MRI                                                                |
| LEN                                                                         | Synthetic_SOP                                                                     |
| MG                                                                          | Mammo_MEG                                                                         |
| MR                                                                          | BMN_Pilot (and many more)                                                         |
| NM                                                                          | Card_001                                                                          |
| OAM                                                                         | Synthetic_SOP                                                                     |
| OP                                                                          | SPEC_OP_OPT                                                                       |
| OPT                                                                         | SPEC_OP_OPT                                                                       |
| OPV                                                                         | Synthetic_SOP                                                                     |
| PT (PET)                                                                    | IHE_MESA_test, PETCT_X9, SEG_DEMO                                                 |
| PR                                                                          | Fruit_Struct                                                                      |
| REG                                                                         | TCIA_REG                                                                          |
| RF                                                                          | Q0L_BW                                                                            |
| RTDOSE                                                                      | SlicerRtData                                                                      |
| RTIMAGE                                                                     | TCIA_RT, Text_Detect                                                              |
| RTPLAN                                                                      | TCIA_RT, Text_Detect                                                              |
| RTRECORD                                                                    | SlicerRtData                                                                      |
| RTSTRUCT                                                                    | TCIA_RT, Text_Detect                                                              |
| RWV                                                                         | QIN-HEADNECK                                                                      |
| SEG                                                                         | BMN_Pilot, SEG_DEMO                                                               |
| SM                                                                          | Q0L_BW                                                                            |
| SMR                                                                         | Synthetic_SOP                                                                     |
| SR                                                                          | R9Q_BW, QIN-HEADNECK                                                              |
| SRF                                                                         | Synthetic_SOP                                                                     |
| US                                                                          | ALI_US_X8W, IHE_MESA_test                                                         |
| VA                                                                          | Synthetic_SOP                                                                     |
| XA                                                                          | Angio_XA                                                                          |
| XC                                                                          | Q0L_BW                                                                            |


| I'm looking a particular type of XNAT data/concept...                       | Find it in...                                                                     |
|-----------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| Containers                                                                  | Nifti_Doc (plugin required)                                                       |
| Custom User Groups                                                          | MultiProcArc (plugin required)                                                    |
| Custom Variables                                                            | CV_Demo                                                                           |
| Event Subscriptions                                                         | EventDemo (plugin required)                                                       |
| Non-imaging subject assessors                                               | Behavior_DB                                                                       |
| Pipeline runs (DicomToNifti)                                                | Nifti_Proc (requires dcm2nii executable on XNAT's executable PATH)                |
| Project-level anonymization                                                 | BJC_Pat (has anonymization), BJC_Pat_Res (identical data without anonymization)   |
| Reconstructions                                                             | GBM_Recon                                                                         |
| Session assessors                                                           | QC_Review (contains Manual QCs), Rad_Review (contains Rad Reads; plugin required) |
| Session assessors w/ data (FreeSurfer)                                      | FS_Prot_IRBM1, MultiProcArc (plugin required)                                     |
| Session assessors w/ data (WMH)                                             | WhiteMatter_P8, MultiProcArc (plugin required)                                    |
| Sharing (umbrella project structure)                                        | UXE, UXE_London, UXE_NYC, UXE_Seoul                                               |
| Sharing (subject pool structure)                                            | SPP, SPP_MTXV_T, SPP_NS, SPP_QXZ-91_T                                             |
| Subject, session & custom scan resources                                    | ResourceDemo                                                                      |


| I'm looking for an edge case...                                             | Find it in...                                                                     |
|-----------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| Study with a large number of series (>100)                                  | SEG_DEMO (session: 4482356)                                                       |
| Repeated series numbers (e.g. scan IDs like 1000-OT1)                       | SEG_DEMO (session: 4482356)                                                       |
| Missing series numbers (e.g. scan IDs like 1_3_6_1_4_1_14519...)            | SEG_DEMO (session: QIN-HEADNECK-01-0017)                                          |


| I'm looking for something else...                                           | Find it in...                                                                     |
------------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| DICOM data from variety of scanners                                         | ALI_US_X8W, IHE_MESA_test                                                         |
| DICOM data encoded in a variety of transfer syntaxes                        | TS_Set                                                                            |
| ECAT7 data (non-DICOM)                                                      | ecat_Arc                                                                          |


| I'm looking for some data with the SOP Class UID...                         | SOP Class Name                                                                    | Find it in session (formatted as project/session)...                |
|-----------------------------------------------------------------------------|-----------------------------------------------------------------------------------|---------------------------------------------------------------------|
| 1.2.840.10008.5.1.1.27                                                      | Stored Print Storage SOP Class (Retired)                                          | Q0L_BW/Q0L_BW_s31_SPHC                                              |
| 1.2.840.10008.5.1.1.29                                                      | Hardcopy Grayscale Image Storage SOP Class (Retired)                              | Q0L_BW/Q0L_BW_s31_SPHC                                              |
| 1.2.840.10008.5.1.4.1.1.1                                                   | Computed Radiography Image Storage                                                | chestQ3/LIDC-IDRI-0004_CR                                           |
| 1.2.840.10008.5.1.4.1.1.1.1                                                 | Digital X-Ray Image Storage - For Presentation                                    | chestQ3/LIDC-IDRI-0002_DX                                           |
| 1.2.840.10008.5.1.4.1.1.1.2                                                 | Digital Mammography X-Ray Image Storage - For Presentation                        | Mammo_MEG/009001_MEG                                                |
| 1.2.840.10008.5.1.4.1.1.1.2.1                                               | Digital Mammography X-Ray Image Storage - For Processing                          | R9Q_BW/R9Q_BW_q05_MG                                                |
| 1.2.840.10008.5.1.4.1.1.1.3                                                 | Digital Intra-Oral X-Ray Image Storage - For Presentation                         | Q0L_BW/Q0L_BW_i04_IO                                                |
| 1.2.840.10008.5.1.4.1.1.2                                                   | CT Image Storage                                                                  | PETCT_X9/Jacobs_14-008-M2_PETCT                                     |
| 1.2.840.10008.5.1.4.1.1.2.1                                                 | Enhanced CT Image Storage                                                         | R9Q_BW/R9Q_BW_x29_CT                                                |
| 1.2.840.10008.5.1.4.1.1.2.2                                                 | Legacy Converted Enhanced CT Image Storage                                        | R9Q_BW/R9Q_BW_d13_ENPTCT                                            |
| 1.2.840.10008.5.1.4.1.1.3                                                   | Ultrasound Multi-frame Image Storage (Retired)                                    | Q0L_BW/Q0L_BW_u44_US                                                |
| 1.2.840.10008.5.1.4.1.1.3.1                                                 | Ultrasound Multi-frame Image Storage                                              | ALI_US_X8W/SIDJDD_US2                                               |
| 1.2.840.10008.5.1.4.1.1.4                                                   | MR Image Storage                                                                  | BMN_Pilot/UCSF-BR-01_v00                                            |
| 1.2.840.10008.5.1.4.1.1.4.1                                                 | Enhanced MR Image Storage                                                         | Fruit_Struct/Dragon_Fruit_MR1                                       |
| 1.2.840.10008.5.1.4.1.1.4.2                                                 | MR Spectroscopy Storage                                                           | Rad_Review/9-BZ-02V-L_L_00001                                       |
| 1.2.840.10008.5.1.4.1.1.4.4                                                 | Legacy Converted Enhanced MR Image Storage                                        | R9Q_BW/R9Q_BW_l36_LCEMR                                             |
| 1.2.840.10008.5.1.4.1.1.6                                                   | Ultrasound Image Storage (Retired)                                                | TS_Set/1_2_840_10008_1_2_1                                          |
| 1.2.840.10008.5.1.4.1.1.6.1                                                 | Ultrasound Image Storage                                                          | ALI_US_X8W/SIDJDD_US2                                               |
| 1.2.840.10008.5.1.4.1.1.6.2                                                 | Enhanced US Volume Storage                                                        | R9Q_BW/R9Q_BW_a98_US                                                |
| 1.2.840.10008.5.1.4.1.1.7                                                   | Secondary Capture Image Storage                                                   | Mammo_MEG/009001_MEG                                                |
| 1.2.840.10008.5.1.4.1.1.7.1                                                 | Multi-frame Single Bit Secondary Capture Image Storage                            | Q0L_BW/Q0L_BW_e13_MSBSC                                             |
| 1.2.840.10008.5.1.4.1.1.7.2                                                 | Multi-frame Grayscale Byte Secondary Capture Image Storage                        | Q0L_BW/Q0L_BW_q00_MGBSC                                             |
| 1.2.840.10008.5.1.4.1.1.7.3                                                 | Multi-frame Grayscale Word Secondary Capture Image Storage                        | Q0L_BW/Q0L_BW_b03_MGWSC                                             |
| 1.2.840.10008.5.1.4.1.1.7.4                                                 | Multi-frame True Color Secondary Capture Image Storage                            | Q0L_BW/Q0L_BW_v68_NMMTCSC                                           |
| 1.2.840.10008.5.1.4.1.1.9.1.1                                               | 12-lead ECG Waveform Storage                                                      | Q0L_BW/Q0L_BW_e00_ECG                                               |
| 1.2.840.10008.5.1.4.1.1.9.1.2                                               | General ECG Waveform Storage                                                      | R9Q_BW/R9Q_BW_x55_XAECG                                             |
| 1.2.840.10008.5.1.4.1.1.9.2.1                                               | Hemodynamic Waveform Storage                                                      | Q0L_BW/Q0L_BW_w43_HW                                                |
| 1.2.840.10008.5.1.4.1.1.9.4.1                                               | Basic Voice Audio Waveform Storage                                                | R9Q_BW/R9Q_BW_a10_AU                                                |
| 1.2.840.10008.5.1.4.1.1.11.1                                                | Grayscale Softcopy Presentation State Storage                                     | Knee_Pilot/Ingenia-436                                              |
| 1.2.840.10008.5.1.4.1.1.11.4                                                | Blending Softcopy Presentation State Storage                                      | Q0L_BW/Q0L_BW_n59_CTPTPR                                            |
| 1.2.840.10008.5.1.4.1.1.12.1                                                | X-Ray Angiographic Image Storage                                                  | Angio_XA/556342B                                                    |
| 1.2.840.10008.5.1.4.1.1.12.1.1                                              | Enhanced XA Image Storage                                                         | R9Q_BW/R9Q_BW_x55_XAECG                                             |
| 1.2.840.10008.5.1.4.1.1.12.2                                                | X-Ray Radiofluoroscopic Image Storage                                             | Q0L_BW/Q0L_BW_r43_RF                                                |
| 1.2.840.10008.5.1.4.1.1.13.1.1                                              | X-Ray 3D Angiographic Image Storage                                               | R9Q_BW/R9Q_BW_x55_3DXA                                              |
| 1.2.840.10008.5.1.4.1.1.13.1.2                                              | X-Ray 3D Craniofacial Image Storage                                               | Q0L_BW/Q0L_BW_c12_CF                                                |
| 1.2.840.10008.5.1.4.1.1.13.1.3                                              | Breast Tomosynthesis Image Storage                                                | R9Q_BW/R9Q_BW_c93_COIN_PHANTOM                                      |
| 1.2.840.10008.5.1.4.1.1.20                                                  | Nuclear Medicine Image Storage                                                    | Card_001/NM_Card_NM1                                                |
| 1.2.840.10008.5.1.4.1.1.30                                                  | Parametric Map Storage                                                            | Q0L_BW/Q0L_BW_p33_PM                                                |
| 1.2.840.10008.5.1.4.1.1.66                                                  | Raw Data Storage                                                                  | Fruit_Struct/Dragon_Fruit_MR1                                       |
| 1.2.840.10008.5.1.4.1.1.66.1                                                | Spatial Registration Storage                                                      | SlicerRtData/H00032-ID20100222_CTRT                                 |
| 1.2.840.10008.5.1.4.1.1.66.2                                                | Spatial Fiducials Storage                                                         | SlicerRtData/H00032-ID20100222_CTRT                                 |
| 1.2.840.10008.5.1.4.1.1.66.3                                                | Deformable Spatial Registration Storage                                           | TCIA_REG/HN-CHUM-002_43knhuz1smrklompp68ff5tz1                      |
| 1.2.840.10008.5.1.4.1.1.66.4                                                | Segmentation Storage                                                              | SEG_DEMO/QIN-HEADNECK-01-0028                                       |
| 1.2.840.10008.5.1.4.1.1.66.5                                                | Surface Segmentation Storage                                                      | Q0L_BW/Q0L_BW_s18_SSS                                               |
| 1.2.840.10008.5.1.4.1.1.66.6                                                | Tractography Results Storage                                                      | Q0L_BW/Q0L_BW_t35_TS                                                |
| 1.2.840.10008.5.1.4.1.1.67                                                  | Real World Value Mapping Storage                                                  | SEG_DEMO/QIN-HEADNECK-01-0028                                       |
| 1.2.840.10008.5.1.4.1.1.77.1.1.1                                            | Video Endoscopic Image Storage                                                    | Q0L_BW/Q0L_BW_v10_ES                                                |
| 1.2.840.10008.5.1.4.1.1.77.1.4                                              | VL Photographic Image Storage                                                     | Q0L_BW/Q0L_BW_o45_VLPI                                              |
| 1.2.840.10008.5.1.4.1.1.77.1.4.1                                            | Video Photographic Image Storage                                                  | Q0L_BW/Q0L_BW_h88_VPI                                               |
| 1.2.840.10008.5.1.4.1.1.77.1.5.1                                            | Ophthalmic Photography 8 Bit Image Storage                                        | SPEC_OP_OPT/SP3                                                     |
| 1.2.840.10008.5.1.4.1.1.77.1.5.3                                            | Stereometric Relationship Storage                                                 | Synthetic_SOP/21_StereometricRelationship                           |
| 1.2.840.10008.5.1.4.1.1.77.1.5.4                                            | Ophthalmic Tomography Image Storage                                               | SPEC_OP_OPT/SP3                                                     |
| 1.2.840.10008.5.1.4.1.1.77.1.6                                              | VL Whole Slide Microscopy Image Storage                                           | Q0L_BW/Q0L_BW_d03_SM                                                |
| 1.2.840.10008.5.1.4.1.1.78.1                                                | Lensometry Measurements Storage                                                   | Synthetic_SOP/93_OphthalmicMeasurements                             |
| 1.2.840.10008.5.1.4.1.1.78.2                                                | Autorefraction Measurements Storage                                               | Synthetic_SOP/93_OphthalmicMeasurements                             |
| 1.2.840.10008.5.1.4.1.1.78.3                                                | Keratometry Measurements Storage                                                  | Synthetic_SOP/93_OphthalmicMeasurements                             |
| 1.2.840.10008.5.1.4.1.1.78.4                                                | Subjective Refraction Measurements Storage                                        | Synthetic_SOP/93_OphthalmicMeasurements                             |
| 1.2.840.10008.5.1.4.1.1.78.5                                                | Visual Acuity Measurements Storage                                                | Synthetic_SOP/93_OphthalmicMeasurements                             |
| 1.2.840.10008.5.1.4.1.1.78.7                                                | Ophthalmic Axial Measurements Storage                                             | Synthetic_SOP/93_OphthalmicMeasurements                             |
| 1.2.840.10008.5.1.4.1.1.78.8                                                | Intraocular Lens Calculations Storage                                             | Synthetic_SOP/93_OphthalmicMeasurements                             |
| 1.2.840.10008.5.1.4.1.1.80.1                                                | Ophthalmic Visual Field Static Perimetry Measurements Storage                     | Synthetic_SOP/93_OphthalmicMeasurements                             |
| 1.2.840.10008.5.1.4.1.1.88.11                                               | Basic Text SR Storage                                                             | Q0L_BW/Q0L_BW_z64_BTSR                                              |
| 1.2.840.10008.5.1.4.1.1.88.22                                               | Enhanced SR Storage                                                               | Q0L_BW/Q0L_BW_d13_ENSR                                              |
| 1.2.840.10008.5.1.4.1.1.88.33                                               | Comprehensive SR Storage                                                          | Hosp_PACS/patient_00x8_US                                           |
| 1.2.840.10008.5.1.4.1.1.88.50                                               | Mammography CAD SR Storage                                                        | R9Q_BW/R9Q_BW_b44_MGCAD                                             |
| 1.2.840.10008.5.1.4.1.1.88.59                                               | Key Object Selection Document Storage                                             | QIN_Breast_DCE-MRI/QIN-Breast-DCE-MRI-BC12_vlpv4hjicr94m6vmcg2imt3v |
| 1.2.840.10008.5.1.4.1.1.88.67                                               | X-Ray Radiation Dose SR Storage                                                   | R9Q_BW/R9Q_BW_p73_SR                                                |
| 1.2.840.10008.5.1.4.1.1.90.1                                                | Content Assessment Results Storage                                                | Synthetic_SOP/12_ContentAssessmentFailed                            |
| 1.2.840.10008.5.1.4.1.1.104.1                                               | Encapsulated PDF Storage                                                          | Hosp_PACS/patient_00x8_US                                           |
| 1.2.840.10008.5.1.4.1.1.104.2                                               | Encapsulated CDA Storage                                                          | Enc_CDA_Sample/Jones_Isabella_History_and_Physical                  |
| 1.2.840.10008.5.1.4.1.1.128                                                 | Positron Emission Tomography Image Storage                                        | PETCT_X9/Jacobs_14-008-M2_PETCT                                     |
| 1.2.840.10008.5.1.4.1.1.128.1                                               | Legacy Converted Enhanced PET Image Storage                                       | R9Q_BW/R9Q_BW_d13_ENPTCT                                            |
| 1.2.840.10008.5.1.4.1.1.131                                                 | Basic Structured Display Storage                                                  | Synthetic_SOP/44_BasicStructuredDisplay                             |
| 1.2.840.10008.5.1.4.1.1.481.1                                               | RT Image Storage                                                                  | Text_Detect/Patient_35                                              |
| 1.2.840.10008.5.1.4.1.1.481.2                                               | RT Dose Storage                                                                   | TCIA_RT/STS_014_1hhnsg68xn3mewbwdobqso8jd                           |
| 1.2.840.10008.5.1.4.1.1.481.3                                               | RT Structure Set Storage                                                          | TCIA_RT/STS_014_1hhnsg68xn3mewbwdobqso8jd                           |
| 1.2.840.10008.5.1.4.1.1.481.5                                               | RT Plan Storage                                                                   | TCIA_RT/STS_014_1hhnsg68xn3mewbwdobqso8jd                           |
| 1.2.840.10008.5.1.4.1.1.481.7                                               | RT Treatment Summary Record Storage                                               | SlicerRtData/H00032-ID20100222_CTRT                                 |
| 1.2.840.10008.5.1.4.1.1.481.8                                               | RT Ion Plan Storage                                                               | SlicerRtData/H00032-ID20100222_CTRT                                 |
| 1.2.840.10008.5.1.4.1.1.481.9                                               | RT Ion Beams Treatment Record Storage                                             | SlicerRtData/H00032-ID20100222_CTRT                                 |
| 1.3.12.2.1107.5.9.1                                                         | Siemens CSA Non-Image Storage (Private)                                           | SlicerRtData/H00032-ID20100222_CTRT                                 |
| 1.3.46.670589.11.0.0.12.2                                                   | Philips MR Series Data Storage (Private)                                          | Knee_Pilot/Ingenia-436                                              |
| 1.3.46.670589.11.0.0.12.4                                                   | Philips MR ExamCard Storage (Private)                                             | Knee_Pilot/Ingenia-436                                              |

The following plugins are prerequisites for some projects. Usually, the source code is available online, or the compiled plugin can be downloaded from the [XNAT maven server](https://nrgxnat.jfrog.io/nrgxnat).

| Plugin                     | Dependent Projects                                    | Source Code                                                          | Maven Group and Artifact ID                                                                     |
|----------------------------|-------------------------------------------------------|----------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
| XNAT Container Service     | EventDemo, Nifti_Doc                                  | https://github.com/NrgXnat/container-service                         | org.nrg:containers                                                                              |
| FreeSurfer Common          | FS_Prot_IRBM1, MultiProcArc                           | https://bitbucket.org/nrg_customizations/nrg_plugin_freesurfercommon | org.nrg.xnat.plugin:freesurfer-common                                                           |
| XNAT Radiology Read        | Rad_Review                                            | https://bitbucket.org/nrg_customizations/nrg_plugin_radread          | org.nrg.xnat.plugin:rad-read                                                                    |
| XNAT WMH                   | MultiProcArc, WhiteMatter_P8                          | https://bitbucket.org/nrg_customizations/nrg_plugin_wmh              | org.nrg.xnat.plugin:wmh                                                                         |
