# XNAT Populate Cheat Sheet

This contains a list of all of the projects available by default with XNAT Populate, and their uses. Any data imported from the default XNAT Populate project were obtained from publicly accessible sources on the web unless explicitly marked otherwise. The data is being used for demo/dev purposes to demonstrate the capabilities of XNAT. Nevertheless, if you come across the data and are interested in it, the original source of the data (DICOM only) can be found inserted into the (0008,4000) _Identifying Comments_ tag in the DICOM (various DICOM headers may have been modified to make the data easier to use with XNAT). The data found in this demo server should probably not actually be used; instead, go to the original source of the data. The data available is:

__ALI_US_X8W__: Contains ALI Technologies Scanner data. Reason for inclusion: scanner diversity, modality diversity (US/Ultrasound).

__Angio_XA__: Contains Xray Angiogram imaging data. Reason for inclusion: modality diversity (XA).

__Behavior_DB__: Contains various non-imaging subject assessors (shipped with XNAT by default, but not set up by default) focusing on clinical-style forms. Reason for inclusion: non-imaging subject assessors, added data types.

__BJC_Pat/BJC_Pat_Res__: These two projects contain DICOM that was identical on upload. The difference is that BJC_Pat contains an anonymization script, while BJC_Pat_Res does not. Hence, the BJC_Pat_Res project contains sessions with “PHI” (faked PHI of Charlie, Jenny, and Rick), while the corresponding data can be viewed in BJC_Pat to see how the anonymization script added removed the PHI. Reason for inclusion: anonymization.

__BMN_Pilot__: Contains data from the TCIA’s Breast-MRI-NACT-Pilot project. Reason for inclusion: data quality, highly realistic data, realistic project description, modality diversity (SEG modality scans under MR session).

__Card_001__: Contains cardiac imaging data. Reason for inclusion: modality diversity (NM modality).

__Cat_Imaging__: Contains cat pictures converted into DICOM files. Reason for inclusion: highly synthetic data, I like cats.

__chestQ3__: Contains chest imaging data with CT, CR, and DX modalities. Reason for inclusion: modality diversity (CT, CR, DX).

__CV_Demo__: Contains imaging data with custom variables at the subject and MR session levels. Reason for inclusion: custom variables.

__Enc_CDA_Sample__: Contains HL7 CDA documents encapsulated into DICOM objects. Reason for inclusion: SOP class diversity.

__ecat_Arc__: Contains ECAT7 PET imaging data (converted from DICOM). Reason for inclusion: data diversity (ECAT).

__EventDemo__: Uses XNAT's Event Service and Container Service features to set up dcm2niix to run in a container for all scans with CT modality that are archived. Requires Container Service Plugin. Reason for inclusion: event services.

__Fruit_Struct__: Contains imaging data of fruits and vegetables in an MRI scanner. Reason for inclusion: the data is really cool.

__FS_Prot_IRBM1__: Contains FreeSurfer session assessors under MR Sessions. Requires NRG FreeSurfer Common Plugin. Reason for inclusion: session assessors with files, FreeSurfer, data types from plugin.

__GBM_Recon__: Contains a few imaging sessions with (XNAT) reconstructions. The reconstructions have an 'IMAGE' resource with essentially a screencapture of a meaningless annotation I made. Reason for inclusion: reconstructions.

__Hosp_PACS__: Contains ultrasound imaging data that was simulated patient data. Reason for inclusion: clinical data.

__IHE_MESA_test__: Contains variety of IHE MESA Software test data. Reason for inclusion: modality diversity (US, CT, PT, CR), scanner diversity: Philips, Fuji, Siemens.

__Knee_Pilot__: Contains knee imaging data. Reason for inclusion: modality diversity (PR scans in MR session).

__Mammo_MEG__: Contains breast imaging data. Reason for inclusion: modality diversity (MG).

__MultiProcArc__: Contains FreeSurfer, WMH, and Manual QC session assessors, where access is defined by custom user groups. Requires NRG FreeSurfer Common Plugin and WMH plugin. Reason for inclusion: session assessors with files, custom user groups.

__Nifti_Doc__: Runs xnat/dcm2niix container on all scans in this project. Requires Container Service plugin. Reason for inclusion: containers.

__Nifti_Proc__: Contains BOGUS imaging data from XNAT Workshop 2012. Any subjects labeled \*001 have DicomToNifti run on all their sessions, while subjects labeled \*002 do not, so the results of a pipeline, or launching a new one can be demonstrated. So, for example, GSU001_v00_mr would already have Nifti generated, but GSU002_v01_mr would not. Reason for inclusion: pipelines (DicomToNifti).

__NIHCC_Chest__: Contains a very large data set (~30000 patients and ~120000 sessions) made public by NIHCC. A simple session assessor is used to represent the findings in the chest xray imaging data. Reason for inclusion: big data.

__OT_Arc_001__: Contains various “other modality” imaging data. Reason for inclusion: modality diversity (OT), public XNAT project accessibility.

__PETCT_X9__: Contains various PET-CT imaging data. Reason for inclusion: data quality, modality diversity (PET, CT), moderately large number of files.

__Q0L_BW__: Contains various DICOM studies with a wide variety of rare SOP classes. Reason for inclusion: SOP class diversity (many rare SOP classes), modality diversity (ES, HC, HD, IO, RF, SM, XC).

__QC_Review__: Contains some DICOM sessions with attached Manual QCs. Reason for inclusion: session assessors (Manual QC).

__QIN_Breast_DCE-MRI__:  Contains some DICOM studies from the QIN Breast DCE-MRI collection in TCIA. Reason for inclusion: modality diversity (KO).

__QIN-HEADNECK__: Contains some DICOM studies from the QIN-HEADNECK TCIA collection where the studies have series of modality RWV or SR. Reason for inclusion: modality diversity (RWV, SR).

__R9Q_BW__: Contains various DICOM studies with a wide variety of rare SOP classes. Reason for inclusion: SOP class diversity (many rare SOP classes), modality diversity (ECG, AU).

__Rad_Review__: Contains some DICOM sessions with attached Radiology Reads. Requires XNAT Radiology Read plugin. Reason for inclusion: session assessors (Radiology Read), data type from plugin.

__ResourceDemo__: Contains mocked ECG sessions from some simple text files and jpegs. Also includes a couple of medical history forms as subject resources and a nonsense calculation PDF as session resources.. Reason for inclusion: subject resources, session resources, custom scan resources (custom snapshots).

__SEG_DEMO__: Contains segmentation (SEG) modality data to demonstrate XNAT’s recently improved ability to handle this type of data. More specifically:

* UCSF* sessions are MR sessions with SEG scans.
* QIN-HEADNECK* sessions are PET/CT sessions with SEG scans.
* 4482356 is a CT scan with a large number (100+!) of SEG scans.

Reason for inclusion: modality diversity (SEG), SEG demo.

__SlicerRtData__: Contains some of the SlicerRtData testing data. Reason for inclusion: modality diversity (FID, RTIMAGE, RTRECORD), SOP class diversity.

__SPEC_OP_OPT__: Contains Spectralis test data. Reason for inclusion: modality diversity (OP, OPT).

__SPP/SPP_MTXV_T/SPP_NS/SPP_QXZ-91_T__: This is a simulated subject pool sharing structure. SPP is the main subject pool, which contains no actual data. The relevant subjects are shared out into projects where the data is collected. Additional accounts are added, where some only have access to individual projects in the SPP span (for example, uxeSppMember has member access to the main pool SPP, and no access to the other projects) to demonstrate access controls. The data in SPP_NS is shared back into the main subject pool project, but this is not so for SPP_MTXV_T and SPP_QXZ-91_T data (to demonstrate this, but to show it's not required). Reason for inclusion: sharing, subject pool.

__Synthetic_SOP__: This collection contains two types of data: (1) relatively normal SOP instances from various sources, and (2) test data of various rarer SOP classes. Instances from (1) can be identified from the source being burned-in to (0008,4000) Identifying Comments, as usual. Instances from (2) can be identified from the Manufacturer (0008,0070) value of 'XNAT'. These instances were created simply to have examples, and as such, the data is likely to be extremely unreasonable. Reason for inclusion: modality and SOP class diversity.

__TCIA_REG__: This contains CT imaging data along with "Deformable Spatial Registration" DICOM objects to capture contours (https://doi.org/10.7937/K9/TCIA.2017.8oje5q00). Reason for inclusion: REG data.

__TCIA_RT__: This contains PET/MR and CT studies with associated RT data (RTSTRUCT) from TCIA. Reason for inclusion: RTSTRUCT data, TCIA integration.

__Text_Detect__: Contains RSNA2000 dataset to provide a demo sample for the text detection pipeline. Reason for inclusion: text detection pipeline, modality diversity (RTIMAGE, RTSTRUCT, RTPLAN scans in OT Session).

__TS_Set__: Contains various DICOM images encoded in a wide variety of Transfer Syntaxes. Reason for inclusion: transfer syntax diversity.

__UXE/UXE_London/UXE_NYC/UXE_Seoul__: This is a simulated umbrella project. UXE is the main project (umbrella project), and the data is uploaded into the other projects (the sites) with some labels. The data (subjects, sessions) is then shared into the main project and reassigned labels matching the sites. Additional accounts are added, where some only have access to individual projects in the UXE span (for example, uxeSeoulMember has member access to UXE_Seoul, and no access to the other projects) to demonstrate access controls. Reason for inclusion: sharing, umbrella project.

__WhiteMatter_P8__: Contains MR sessions with WhiteMatterHyperintensitiesSegmentation (WMH) assessors. Requires XNAT WMH plugin. Reason for inclusion: session assessors with files, WMH, data types from plugin, synthetic scans. 

An up-to-date copy of this file can always be found [here](https://bitbucket.org/xnatdev/xnat_populate/src/master/xnat_populate_cheat_sheet.md?fileviewer=file-view-default).
