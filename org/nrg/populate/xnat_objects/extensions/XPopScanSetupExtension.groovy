package org.nrg.populate.xnat_objects.extensions

import com.jayway.restassured.http.ContentType
import org.nrg.populate.util.ConsoleUtils
import org.nrg.populate.util.Globals
import org.nrg.populate.xnat_objects.ContainerRegistry
import org.nrg.xnat.pogo.Extension
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.containers.Wrapper
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan

class XPopScanSetupExtension extends Extension<Scan> {

    Map<String, Map<String, String>> containers

    XPopScanSetupExtension(Scan scan) {
        super(scan)
    }

    void launchContainers(Project project, ImagingSession session) {
        containers.each { uniqueAlias, containerParams ->
            final Wrapper wrapper = ContainerRegistry.knownWrappers().find { it.uniqueAlias == uniqueAlias }
            if (wrapper == null) {
                ConsoleUtils.warn("Cannot run container wrapper identified as '${uniqueAlias}' because it was not defined properly.")
            } else {
                containerParams.each { key, val ->
                    containerParams[key] = val.replace('$sessionId', session.getAccessionNumber()).replace('$scanLabel', parentObject.id)
                }
                Globals.xnatClient.queryBase().contentType(ContentType.JSON).body(containerParams).post(Globals.xnatClient.formatXapiUrl("/projects/${project}/wrappers/${wrapper.id}/launch")).then().assertThat().statusCode(200)
                println("Successfully launched container with wrapper ${uniqueAlias} on scan ${parentObject.id} for session ${session}.")
            }
        }
    }

}
