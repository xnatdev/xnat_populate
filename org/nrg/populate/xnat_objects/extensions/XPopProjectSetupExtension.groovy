package org.nrg.populate.xnat_objects.extensions

import org.nrg.populate.injector.XnatParserInjector
import org.nrg.populate.tcia.TciaSpec
import org.nrg.populate.util.Globals
import org.nrg.populate.util.YamlUtils
import org.nrg.populate.xnat_collections.Pipelines
import org.nrg.populate.xnat_objects.ContainerRegistry
import org.nrg.populate.xnat_objects.Pipeline
import org.nrg.xnat.pogo.AnonScript
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.containers.Command
import org.nrg.xnat.pogo.containers.Image
import org.nrg.xnat.pogo.containers.Wrapper
import org.nrg.xnat.pogo.events.Subscription
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.extensions.project.ProjectQueryPutExtension
import org.nrg.xnat.util.ListUtils

import java.time.LocalDate

class XPopProjectSetupExtension extends ProjectQueryPutExtension {

    String src
    String remoteSource
    String dataPath
    AnonScript anonScript
    List<String> requiredCommandWrappers = []
    List<Image> imageDefinitions = []
    LocalDate lastUpdated
    List<String> projectDependencies = []
    TciaSpec tciaSpec
    List<XnatParserInjector> injectors = []
    // TODO: secondarySubjects

    private Map<String, Map<String, String>> pipelineMaps = [:]
    private final List<Pipeline> pipelines = []
    private final List<String> pluginDependencies = []

    XPopProjectSetupExtension(Project project) {
        super(Globals.xnatClient, project)
    }

    Map<String, Map<String, String>> getPipelineMaps() {
        pipelineMaps
    }

    void setPipelineMaps(Map<String, Map<String, String>> pipelineMaps) {
        if (pipelineMaps != null) this.pipelineMaps = pipelineMaps
    }

    List<Pipeline> getPipelines() {
        pipelines
    }

    void setPipelines(List<Pipeline> pipelines) {
        ListUtils.copyInto(pipelines, this.pipelines)
    }

    List<String> getPluginDependencies() {
        pluginDependencies
    }

    void setPluginDependencies(List<String> pluginDependencies) {
        ListUtils.copyInto(pluginDependencies, this.pluginDependencies)
    }

    boolean requiresContainerService() {
        parentObject.subscriptions.any { it.actionKey.startsWith(Subscription.COMMAND_ACTION_PROVIDER) } || requiredCommandWrappers.size() > 0 || parentObject.subjects.any { subject ->
            subject.getExperiments().any { experiment ->
                (experiment instanceof ImagingSession) && (experiment as ImagingSession).scans.any { scan ->
                    (scan.extension != null) && (scan.extension as XPopScanSetupExtension).containers.size() > 0
                }
            }
        }
    }

    boolean hasPluginDependencies() {
        !pluginDependencies.isEmpty()
    }

    void readPipelines() {
        pipelineMaps.each { key, value ->
            final Pipeline pipeline = (Pipelines.has(key)) ? Pipelines.getPipeline(key) : YamlUtils.deserializePipeline(key)
            if (value != null) pipeline.putAll(value)
            pipeline.put('project', parentObject.id)
            pipelines << pipeline
        }
    }

    void enableCommands() {
        if (requiredCommandWrappers.size() > 0) {
            requiredCommandWrappers.each { uniqueAlias ->
                final Wrapper requiredWrapper = ContainerRegistry.knownWrappers().find { it.uniqueAlias == uniqueAlias }
                final Command command = ContainerRegistry.knownCommands().find { possibleCommand ->
                    possibleCommand.wrappers.any { wrapper ->
                        wrapper.id == requiredWrapper.id
                    }
                }
                Globals.xnatClient.queryBase().put(Globals.xnatClient.formatXapiUrl("/projects/${parentObject.id}/commands/${command.id}/wrappers/${requiredWrapper.name}/enabled")).then().assertThat().statusCode(200)
                println("Enabled command wrapper ${requiredWrapper.name} (unique alias '${uniqueAlias}') on project ${parentObject}...")
            }
        }
    }

    @Override
    void create() {
        super.create()
        if (anonScript != null) {
            Globals.xnatClient.setProjectAnonScript(parentObject, anonScript)
            Globals.xnatClient.enableProjectAnonScript(parentObject)
        }
        readPipelines()
        pipelines.each { pipeline ->
            pipeline.addToProject()
        }
        enableCommands()
    }

}
