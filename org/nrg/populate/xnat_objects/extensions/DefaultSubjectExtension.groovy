package org.nrg.populate.xnat_objects.extensions

import org.nrg.populate.util.Globals
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.extensions.subject.SubjectQueryPutExtension

class DefaultSubjectExtension extends SubjectQueryPutExtension {

    DefaultSubjectExtension(Subject subject) {
        super(Globals.xnatClient, subject)
    }

    @Override
    void create(Project project) {
        super.create(project)
        println("Subject '${parentObject}' successfully created.")
    }

}
