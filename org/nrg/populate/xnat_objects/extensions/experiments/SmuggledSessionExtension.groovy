package org.nrg.populate.xnat_objects.extensions.experiments

import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan

/*
  Due to XNAT-4527, XNAT cannot import a variety of DICOM data natively. To get around this, some of the studies in this project have a simple secondary capture object
  inserted into them with patient/study data copied from the real DICOM so that XNAT can build a session object during the import. This extension imports the DICOM in this way
  and then deletes the bogus secondary capture object once the import is complete.
 */
class SmuggledSessionExtension extends DefaultSessionExtension {

    SmuggledSessionExtension(ImagingSession session) {
        super(session)
    }

    @Override
    void create(Project project, Subject subject) {
        super.create(project, subject) // add all data
        xnatInterface.deleteScan(project, subject, parentObject as ImagingSession, new Scan().id('TEMP'))
    }

}


