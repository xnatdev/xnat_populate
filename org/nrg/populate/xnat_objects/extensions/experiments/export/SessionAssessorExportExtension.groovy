package org.nrg.populate.xnat_objects.extensions.experiments.export

import com.fasterxml.jackson.databind.JsonSerializer
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.extensions.session_assessor.SessionAssessorExtension

abstract class SessionAssessorExportExtension extends SessionAssessorExtension {

    SessionAssessorExportExtension() {
        super(null)
    }

    @Override
    void create(Project project, Subject subject, ImagingSession session) {
        throw new UnsupportedOperationException('This family of extensions is used for exporting in XPOP.')
    }

    abstract JsonSerializer serializer()

}
