package org.nrg.populate.xnat_objects.extensions.experiments.export

import com.fasterxml.jackson.databind.JsonSerializer
import org.nrg.populate.jackson.serializers.XPopExportSimpleSessionAssessorSerializer
import org.nrg.xnat.pogo.extensions.session_assessor.SessionAssessorExtension

class SimpleSessionAssessorExportExtension extends SessionAssessorExportExtension {

    @Override
    JsonSerializer serializer() {
        XPopExportSimpleSessionAssessorSerializer.INSTANCE
    }

}
