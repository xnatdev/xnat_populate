package org.nrg.populate.xnat_objects.extensions.experiments.export

import com.fasterxml.jackson.databind.JsonSerializer
import org.nrg.populate.jackson.serializers.XPopExportNonImporterSessionSerializer

class NonImporterSessionExtension extends SubjectAssessorExportExtension {

    @Override
    JsonSerializer serializer() {
        XPopExportNonImporterSessionSerializer.INSTANCE
    }

}
