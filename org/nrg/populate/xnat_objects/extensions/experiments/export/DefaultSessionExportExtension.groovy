package org.nrg.populate.xnat_objects.extensions.experiments.export

import com.fasterxml.jackson.databind.JsonSerializer
import org.nrg.populate.jackson.serializers.XPopExportSessionSerializer

class DefaultSessionExportExtension extends SubjectAssessorExportExtension {

    @Override
    JsonSerializer serializer() {
        XPopExportSessionSerializer.INSTANCE
    }

}
