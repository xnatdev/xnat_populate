package org.nrg.populate.xnat_objects.extensions.experiments

import com.jayway.restassured.http.ContentType
import org.nrg.populate.util.ConsoleUtils
import org.nrg.populate.util.Globals
import org.nrg.populate.xnat_objects.ContainerRegistry
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.containers.Wrapper
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.SubjectAssessor
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension

class DefaultSessionExtension extends SessionImportExtension {

    String src
    Map<String, Map> pipelines = [:]
    Map<String, Map<String, String>> containers = [:]

    DefaultSessionExtension(ImagingSession session) {
        super(Globals.xnatClient, session, null)
    }

    Map getDefaultPipelineParams(Project project, Subject subject) {
        ['project': project.id, 'subject': xnatInterface.getAccessionNumber(subject), 'xnat_id': xnatInterface.getAccessionNumber(parentObject as SubjectAssessor), 'sessionId': parentObject.label]
    }

    @Override
    void create(Project project, Subject subject) {
        setSessionZip(new File(src ?: "${(project.extension as XPopProjectSetupExtension).dataPath}/${parentObject}.zip"))
        try {
            super.create(project, subject)
            println("Imaging session '${parentObject}' successfully created")
        } catch (Exception | Error e) {
            ConsoleUtils.warn("WARNING: failed to upload session ${parentObject.label} due to: ${e}")
        }
        if (parentObject.notes != null) {
            xnatInterface.queryBase().queryParam('note', parentObject.notes).put(xnatInterface.formatRestUrl("/projects/${project.id}/subjects/${subject.label}/experiments/${parentObject.label}")).then().assertThat().statusCode(200)
        }
        Globals.sleep()

        if (!pipelines.isEmpty()) {
            xnatInterface.waitForAutoRun(parentObject.project(project) as ImagingSession, 600)
        }

        pipelines.each { pipeline, params ->
            final Map pipelineParams = getDefaultPipelineParams(project, subject)
            pipelineParams.putAll(params)

            xnatInterface.queryBase().queryParams(pipelineParams).post(xnatInterface.formatRestUrl("/projects/${project}/pipelines/${pipeline}/experiments/${parentObject}")).then().assertThat().statusCode(200)
            println("Successfully launched pipeline ${pipeline} for session '${parentObject}'.")
        }
        if (parentObject.primaryProject == null) {
            parentObject.setPrimaryProject(project)
        }
    }

    void launchContainers(Project project) {
        containers.each { uniqueAlias, containerParams ->
            final Wrapper wrapper = ContainerRegistry.knownWrappers().find { it.uniqueAlias == uniqueAlias }
            if (wrapper == null) {
                ConsoleUtils.warn("Cannot run container wrapper identified as '${uniqueAlias}' because it was not defined properly.")
            } else {
                containerParams.each { key, val ->
                    containerParams[key] = val.replace('$sessionId', (parentObject as ImagingSession).getAccessionNumber())
                }
                Globals.xnatClient.queryBase().contentType(ContentType.JSON).body(containerParams).post(Globals.xnatClient.formatXapiUrl("/projects/${project}/wrappers/${wrapper.id}/launch")).then().assertThat().statusCode(200)
                println("Successfully launched container with wrapper ${uniqueAlias} on session ${parentObject}.")
            }
        }
    }

}
