package org.nrg.populate.xnat_objects.extensions.experiments

import org.nrg.populate.util.Globals
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.SubjectAssessor
import org.nrg.xnat.pogo.extensions.subject_assessor.SubjectAssessorXMLExtension

class XPopSubjectAssessorXMLExtension extends SubjectAssessorXMLExtension {

    XPopSubjectAssessorXMLExtension(SubjectAssessor experiment) {
        super(Globals.xnatClient, experiment, null)
    }

    @Override
    void create(Project project, Subject subject) {
        setAssessorXML(new File("${(project.extension as XPopProjectSetupExtension).dataPath}/${parentObject}.xml"))
        super.create(project, subject)
        println("Successfully uploaded subject assessor: ${parentObject}")
    }

}
