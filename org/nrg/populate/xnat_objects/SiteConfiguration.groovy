package org.nrg.populate.xnat_objects

import com.jayway.restassured.http.ContentType
import org.hamcrest.Matchers
import org.nrg.populate.util.XMLUtils
import org.nrg.populate.xnat_collections.Pipelines
import org.nrg.populate.util.YamlUtils
import org.nrg.xnat.pogo.AnonScript
import org.nrg.xnat.pogo.DataType

import static org.nrg.populate.util.Globals.xnatClient

class SiteConfiguration {

    Map<String, Map<String, String>> pipelineMaps = [:]
    private List<Pipeline> pipelines = []
    AnonScript anonScript
    Map<String, String> siteConfig = [:]
    List<DataType> dataTypes = []
    List<ReportAction> reportActions = []
    List<String> searches = []

    void readPipelines() {
        pipelineMaps.each { pipelineName, otherData ->
            final Pipeline pipeline = YamlUtils.deserializePipeline(pipelineName)
            if (otherData != null) {
                if (otherData.containsKey('path')) {
                    ['pipe:pipelineDetails.path', 'arc:project_descendant_pipeline.location', 'pipeline_path'].each { key ->
                        pipeline.put(key, otherData['path']) // insert value for 'path' for each of these keys
                    }
                }
                otherData.remove('path')
                pipeline.putAll(otherData)
            }
            pipelines << pipeline
            Pipelines.addPipeline(pipelineName, pipeline)
        }
    }

    void post() {
        postConfig()
        postAnon()
        postPipelines()
        postDataTypes()
        postReportActions()
        postSearches()
    }

    private void postConfig() {
        if (!siteConfig.isEmpty()) {
            xnatClient.queryBase().contentType(ContentType.JSON).body(siteConfig).post(xnatClient.formatXapiUrl('siteConfig')).then().assertThat().statusCode(200)
            println('Properties were successfully posted to /xapi/siteConfig')
        }
    }

    private void postAnon() {
        if (anonScript != null) {
            xnatClient.enableSiteAnonScript()
            xnatClient.setSiteAnonScript(anonScript)
        }
    }

    private void postPipelines() {
        readPipelines()
        pipelines.each { pipeline ->
            pipeline.addToSite()
        }
    }

    private void postDataTypes() {
        if (!dataTypes.isEmpty()) {
            final String genericDataType = new File('data_types/generic_data_type.yaml').text

            dataTypes.each { dataType ->
                final String dataTypeYaml = genericDataType.replace('$xsiType', dataType.xsiType).replace('$code', dataType.code).replace('$singularName', dataType.singularName).replace('$pluralName', dataType.pluralName)

                xnatClient.requestWithCsrfToken().contentType(ContentType.URLENC).formParams(YamlUtils.deserializeString(dataTypeYaml, Map)).post(xnatClient.formatXnatUrl('app/action/ElementSecurityWizard')).
                        then().assertThat().statusCode(200)
                println("Successfully set up data type: ${dataType.xsiType}...")
            }
        }
    }

    private void postReportActions() {
        if (!reportActions.isEmpty()) {
            final String genericReportAction = new File('data_types/generic_report_action.yaml').text

            reportActions.each { reportAction ->
                reportAction.xsiTypes.each { xsiType ->
                    final String reportActionYaml = genericReportAction.replace('$xsiType', xsiType).replace('$name', reportAction.name).replace('$displayName', reportAction.displayName).replace('$popup', reportAction.popup).replace('$secureAccess', reportAction.secureAccess)

                    xnatClient.requestWithCsrfToken().contentType(ContentType.URLENC).formParams(YamlUtils.deserializeString(reportActionYaml, Map)).post(xnatClient.formatXnatUrl('/app/action/ModifyItem')).
                        then().assertThat().statusCode(Matchers.isOneOf(200, 302))
                    println("Successfully added '${reportAction.name}' report action to ${xsiType} data type.")
                }
            }
        }
    }

    private void postSearches() {
        if (!searches.isEmpty()) {
            searches.each { search ->
                final String searchXML = XMLUtils.readXML("stored_searches/${search}.xml")

                xnatClient.queryBase().contentType(ContentType.XML).body(searchXML).put(xnatClient.formatRestUrl("search/saved/${search}")).then().assertThat().statusCode(200)

                println("Successfully added stored search: ${search}...")
            }
        }
    }

}
