package org.nrg.populate.xnat_objects.experiments

import org.nrg.xnat.pogo.experiments.SessionAssessor

class SimpleSessionAssessor extends SessionAssessor {

    SimpleSessionAssessor(String label) {
        super()
        setLabel(label)
    }

}
