package org.nrg.populate.xnat_objects.experiments

import org.nrg.xnat.pogo.experiments.ImagingSession

class SimpleSession extends ImagingSession {

    SimpleSession(String session) {
        super(null, null, session)
    }

}
