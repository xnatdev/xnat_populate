package org.nrg.populate.xnat_objects.experiments

import org.nrg.xnat.pogo.experiments.ImagingSession

/**
 * 'Empty' in the sense that the session is created through the experiments API with things added later (instead of via session REST import)
 */

class NonImporterSession extends ImagingSession {}
