package org.nrg.populate

import com.jayway.restassured.http.ContentType
import com.jayway.restassured.response.Response
import groovy.io.FileType
import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.StringUtils
import org.nrg.populate.injector.XnatProjectInjector
import org.nrg.populate.injector.XnatScanInjector
import org.nrg.populate.injector.XnatSessionInjector
import org.nrg.populate.injector.XnatSubjectInjector
import org.nrg.populate.util.ConsoleUtils
import org.nrg.populate.util.Globals
import org.nrg.populate.util.PathUtils
import org.nrg.populate.util.XMLUtils
import org.nrg.populate.util.YamlUtils
import org.nrg.populate.xnat_collections.Users
import org.nrg.populate.xnat_objects.extensions.ResourcePathExtension
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.populate.xnat_objects.extensions.XPopScanSetupExtension
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSessionExtension
import org.nrg.populate.xnat_objects.extensions.experiments.SecondarySubjectSessionExtension
import org.nrg.populate.xnat_objects.extensions.experiments.export.DefaultSessionAssessorExportExtension
import org.nrg.populate.xnat_objects.extensions.experiments.export.DefaultSessionExportExtension
import org.nrg.populate.xnat_objects.extensions.experiments.export.NonImporterSessionExtension
import org.nrg.populate.xnat_objects.extensions.experiments.export.SimpleSessionAssessorExportExtension
import org.nrg.populate.xnat_objects.extensions.experiments.export.SimpleSessionExtension
import org.nrg.testing.CollectionUtils
import org.nrg.xnat.interfaces.XnatInterface
import org.nrg.xnat.pogo.Extension
import org.nrg.xnat.pogo.PluginRegistry
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.XnatPlugin
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.NonimagingAssessor
import org.nrg.xnat.pogo.experiments.Scan
import org.nrg.xnat.pogo.extensions.project.ProjectQueryPutExtension
import org.nrg.xnat.pogo.resources.Resource
import org.nrg.xnat.pogo.resources.ResourceFile

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class XnatDriver {
    private final XnatInterface restClient
    private List<Project> projects
    private final boolean skipUserCreation
    private boolean previousDataDeleted = false

    XnatDriver(String username, String password, String url, boolean insecure, boolean skipUserCreation) {
        restClient = XnatInterface.authenticate(url, username, password, insecure)
        restClient.disableAdminCheck()
        restClient.setServerIssueRetryCount(5)
        Globals.xnatClient = restClient
        this.skipUserCreation = skipUserCreation
    }

    void initialize() {
        projects = Globals.projects
        final Response response = restClient.queryBase().get(restClient.formatXapiUrl('/siteConfig/initialized'))
        if (response.statusCode == 200 && response.asString() == 'true') {
            println('XNAT instance has been initialized previously')
        } else {
            postInit()
        }
    }

    void postInit() {
        restClient.queryBase().contentType(ContentType.JSON).body(['initialized': true, 'siteUrl': StringUtils.stripEnd(restClient.formatXnatUrl(), '/')]).post(restClient.formatXapiUrl('siteConfig')).
            then().assertThat().statusCode(200)

        println('XNAT site initialized...')
    }

    void checkPlugins() {
        projects.each { project ->
            if ((project.extension as XPopProjectSetupExtension).requiresContainerService()) (project.extension as XPopProjectSetupExtension).pluginDependencies << PluginRegistry.CONTAINER_SERVICE.id
        }

        if (projects.any { (it.extension as XPopProjectSetupExtension).hasPluginDependencies() } ) {
            final Set<String> installedPlugins = restClient.readInstalledPlugins().id as Set<String>

            final List<Project> unsupportedProjects = []
            final Set<String> missing = []

            projects.findAll { (it.extension as XPopProjectSetupExtension).hasPluginDependencies() }.each { project ->
                final Set<String> projectRequirements = (project.extension as XPopProjectSetupExtension).pluginDependencies.toSet()
                if (installedPlugins.containsAll(projectRequirements)) {
                    println("XNAT plugin requirements for project ${project} are all satisfied...")
                } else {
                    final Set<String> missingPlugins = projectRequirements - installedPlugins
                    ConsoleUtils.warn("XNAT plugin requirements are not satisfied for project ${project}. This project will NOT be uploaded. Missing plugins: ${missingPlugins}.")
                    missing.addAll(missingPlugins)
                    unsupportedProjects << project
                }
            }

            missing.each { plugin ->
                final XnatPlugin xnatPlugin = PluginRegistry.lookupPlugin(plugin)
                if (xnatPlugin == null) {
                    ConsoleUtils.warn("Unfortunately, I'm not familiar with the ${plugin} plugin, so I can't tell you where you can find it.")
                } else {
                    if (xnatPlugin.mavenArtifactId != null && xnatPlugin.mavenGroupId) {
                        ConsoleUtils.warn("You may be able to find the ${plugin} plugin on the XNAT maven server (${PluginRegistry.XNAT_MAVEN_SERVER}) under the group ${xnatPlugin.mavenGroupId} and artifact ID ${xnatPlugin.mavenArtifactId}.")
                    }
                    if (xnatPlugin.sourceUrl != null) {
                        ConsoleUtils.warn("You may be able to find the source for the ${plugin} plugin at: ${xnatPlugin.sourceUrl}.")
                    }
                }
            }

            projects.removeAll(unsupportedProjects)
        } else {
            println('None of the XNAT projects selected require XNAT plugins. Skipping plugin checks...')
        }
    }

    void checkProjectDependencies() {
        final List<Project> hasDependencies = projects.findAll { (it.extension as XPopProjectSetupExtension).projectDependencies.size() > 0 }
        boolean performNextIteration = true
        while (performNextIteration) {
            performNextIteration = false
            hasDependencies.removeIf { project ->
                final List<String> dependencies = (project.extension as XPopProjectSetupExtension).projectDependencies
                final String missingDependency = dependencies.find { dependency ->
                    !projects.any { it.id == dependency }
                }
                if (missingDependency != null) {
                    ConsoleUtils.warn("Project ${project} has an unfulfilled dependency on project ${missingDependency}, so it will be skipped from upload.")
                    performNextIteration = true
                    projects.remove(project)
                    return true
                } else {
                    return false
                }
            }
        }
        hasDependencies.each { project ->
            println("Project dependencies are all satisfied for project ${project}.")
        }
    }

    void fresh() {
        ConsoleUtils.inform('Deleting existing project data...')
        final List<String> existingProjects = restClient.queryBase().queryParam('format', 'json').get(restClient.formatRestUrl('projects')).jsonPath().getList('ResultSet.Result.ID')
        projects.each { project ->
            if (project.id in existingProjects) {
                ConsoleUtils.inform("Deleting data for project: ${project.id}...")
                restClient.deleteAllProjectData(project)
            }
        }
        previousDataDeleted = true
    }

    void postData() {
        if (!previousDataDeleted) {
            final List<Project> preexistingProjects = Globals.xnatClient.listProjects().findAll { project ->
                project.id in projects.id
            }
            if (!preexistingProjects.isEmpty()) {
                ConsoleUtils.warn("WARNING: some of the projects specified for uploading to XNAT already exist on the server. To populate data for projects that already exist, the -c flag should be used to delete the pre-existing data, since attempting to upload duplicate data will almost certainly cause a failure due to conflicts (HTTP 409). Affected projects: ${preexistingProjects.id}.")
            }
        }
        processInjectors()
        createUsers()

        projects.each { project -> // create each project first so sharing will go fine
            new ProjectQueryPutExtension(Globals.xnatClient, new Project(project.id)).create()
            println("Project '${project}' successfully created")
        }

        projects.each { project ->
            Globals.xnatClient.createProject(project)
        }

        projects.each { project ->
            project.subjects.each { subject ->
                subject.experiments.findAll { experiment ->
                    experiment instanceof ImagingSession
                }.each { session ->
                    final ImagingSession sessionObject = session as ImagingSession
                    if (sessionObject.extension instanceof DefaultSessionExtension) {
                        (sessionObject.extension as DefaultSessionExtension).launchContainers(project)
                    }
                    sessionObject.scans.each { scan ->
                        if (scan.extension instanceof XPopScanSetupExtension) {
                            (scan.extension as XPopScanSetupExtension).launchContainers(project, session as ImagingSession)
                        }
                    }
                }
            }
        }

        projects.each { project ->
            project.secondarySubjects.each { subject ->
                subject.experiments.each { experiment ->
                    experiment.extension(new SecondarySubjectSessionExtension(experiment as ImagingSession))
                    Globals.xnatClient.createSubjectAssessor(project, subject, experiment)
                }
            }
        }
    }

    void createUsers() {
        Set<String> xnatUsers = Users.queryFromXnat().toSet()

        if (!skipUserCreation) {
            final Set<String> allRequiredUsers = []

            projects.each { project ->
                project.users.values().each { userList ->
                    userList.each { user ->
                        allRequiredUsers << user.username
                    }
                }
            }

            final Set<String> usersToCreate = allRequiredUsers - xnatUsers

            //noinspection GroovyAssignabilityCheck
            allRequiredUsers.intersect(xnatUsers).each { user ->
                println("XNAT user '${user}' already exists...")
            }

            usersToCreate.each { user ->
                println("Creating XNAT user ${user}...")
                restClient.createUser(Users.constructDefault(user))
                println("Successfully created XNAT user '${user}'.")
            }
        } else {
            projects.each { project ->
                project.users.values().each { users ->
                    users.clear()
                }
            }
        }
        Users.users = Users.queryFromXnat()
    }

    void processInjectors() {
        projects.each { project ->
            final XPopProjectSetupExtension extension = project.extension as XPopProjectSetupExtension
            final List<XnatProjectInjector> projectInjectors = CollectionUtils.ofType(extension.injectors, XnatProjectInjector)
            final List<XnatSubjectInjector> subjectInjectors = CollectionUtils.ofType(extension.injectors, XnatSubjectInjector)
            final List<XnatSessionInjector> sessionInjectors = CollectionUtils.ofType(extension.injectors, XnatSessionInjector)
            final List<XnatScanInjector> scanInjectors = CollectionUtils.ofType(extension.injectors, XnatScanInjector)

            projectInjectors.each { injector ->
                injector.injectData(project)
            }
            project.subjects.each { subject ->
                subjectInjectors.each { injector ->
                    injector.injectData(project, subject)
                }
                subject.sessions.each { session ->
                    sessionInjectors.each { injector ->
                        injector.injectData(project, subject, session)
                    }
                    session.scans.each { scan ->
                        scanInjectors.each { injector ->
                            injector.injectData(project, subject, session, scan)
                        }
                    }
                }
            }
        }
    }

    void export(List<String> projectList) {
        Paths.get('export').deleteDir()
        Files.createDirectory(Paths.get('export'))
        projectList.each { id ->
            Files.createDirectory(PathUtils.exportDir(id))
        }

        projectList.collect { id ->
            final Project project = restClient.readProject(id)

            project.projectResources.each { resource ->
                parseAndDownloadResourceFiles(resource)
            }

            project.subjects.each { subject ->
                subject.resources.each { resource ->
                    parseAndDownloadResourceFiles(resource)
                }
                subject.experiments.findAll { experiment ->
                    experiment instanceof ImagingSession
                }.each { session ->
                    parseAndDownloadSession(project, subject, session as ImagingSession)
                }
                subject.experiments.findAll { experiment ->
                    experiment instanceof NonimagingAssessor
                }.each { subjectAssessor ->
                    XMLUtils.extractExperimentXmlFromXnat("data/projects/${project}/subjects/${subject}/experiments/${subjectAssessor}", PathUtils.exportDir(project.id).resolve("${subjectAssessor}.xml").toFile())
                    subjectAssessor.extension(new SimpleSessionExtension())
                }
            }

            println("Project ${id} processed from XNAT...")
            project
        }.each { project ->
            YamlUtils.writeToFile(new File("export/${project.id}_metadata.yaml"), project)
        }
    }

    private Path downloadResource(Resource resource) {
        final Path download = Files.createTempFile('XNAT_temp_file', '.zip')

        restClient.saveBinaryResponseToFile(
                restClient.queryBase().queryParam('format', 'zip').get(restClient.formatXnatUrl("${resource.resourceUrl()}/resources/${resource.folder}/files")).then().assertThat().statusCode(200).and().extract().response(),
                download.toFile()
        )

        download
    }

    private void parseAndDownloadResourceFiles(Resource resource) {
        final List<Map> filesResponse = restClient.jsonQuery().get(restClient.formatXnatUrl("${resource.resourceUrl()}/resources/${resource.folder}/files")).jsonPath().get('ResultSet.Result').collect { file ->
            //noinspection GroovyAssignabilityCheck
            file['path'] = file.URI.drop(file.URI.indexOf('files/') + 6) // set path equal to what's after /files/ in the URI
            file
        } as List<Map>

        final List<Map<String, String>> filesInSubdirs, filesNotInSubdirs
        (filesInSubdirs, filesNotInSubdirs) = filesResponse.split { file ->
            file.path.contains('/')
        }

        final Path tempDir = Files.createTempDirectory('XP')
        final Path resourceZip = downloadResource(resource)
        final String resourceDirName = "resource_${RandomStringUtils.randomAlphanumeric(10)}"
        final Path resourceDir = PathUtils.exportDir(resource.project.id).resolve(resourceDirName)

        final Extension<Resource> extension = new ResourcePathExtension(resource)
        extension.setSubFolder(resourceDirName)

        Files.createDirectory(resourceDir)
        Globals.antBuilder.unzip(src: resourceZip.toFile(), dest: tempDir)
        Files.delete(resourceZip)
        final Path unzippedResources = findResourceContents(tempDir).toPath()

        filesNotInSubdirs.each { fileRepresentation ->
            final String name = fileRepresentation.path
            final String content = fileRepresentation.file_content

            Files.copy(unzippedResources.resolve(name), resourceDir.resolve(name))
            Files.delete(unzippedResources.resolve(name))
            final ResourceFile resourceFile = new ResourceFile(resource, name) // TODO: this should have a deserializer
            if (StringUtils.isNotEmpty(content)) resourceFile.setContent(content)
            resource.resourceFiles << resourceFile
        }

        if (!filesInSubdirs.isEmpty()) {
            Globals.antBuilder.zip(basedir: unzippedResources, destfile: resourceDir.resolve('otherfiles.zip'))
            final ResourceFile resourceFile = new ResourceFile(resource, 'otherfiles.zip')
            resourceFile.setUnzip(true)
            resource.resourceFiles << resourceFile
        }

        tempDir.deleteDir()
    }

    private findResourceContents = { Path unzippedResources ->
        File current = unzippedResources.toFile()
        File filesDir = null
        while (true) {
            boolean hasSubdir = false
            current.eachFile(FileType.DIRECTORIES) { file ->
                if (file.name == 'files') {
                    filesDir = file
                }
                current = file
                hasSubdir = true
            }
            if (filesDir != null) return filesDir
            if (!hasSubdir) throw new RuntimeException('Could not find files subdirectory')
        }
    }

    private void parseAndDownloadSession(Project project, Subject subject, ImagingSession session) {
        final List<Scan> importerScans, customScans
        (importerScans, customScans) = session.scans.split { scan ->
            scan.scanResources.any { it.folder in ['DICOM', 'secondary'] } && !scan.scanResources.any { !(it.folder in ['DICOM', 'secondary', 'SNAPSHOTS']) } // importerScan must have DICOM or secondary resources, and can only have DICOM, secondary, or SNAPSHOTS resources
        }

        if (importerScans.isEmpty()) {
            session.extension(new NonImporterSessionExtension())
        } else if (session.resources.isEmpty() && customScans.isEmpty() && session.assessors.isEmpty()) {
            session.extension(new SimpleSessionExtension())
        } else {
            session.extension(new DefaultSessionExportExtension())
        }

        if (!importerScans.isEmpty()) {
            final Path sessionFolder = PathUtils.exportDir(project.id).resolve(session.label) // for simple scans
            final Path sessionZip = PathUtils.exportDir(project.id).resolve("${session.label}.zip") // zipped simple scans
            Files.createDirectory(sessionFolder)

            importerScans.each { scan ->
                scan.scanResources.findAll { it.folder in ['DICOM', 'secondary'] }.each { resource ->
                    final Path zippedResource = downloadResource(resource)
                    final Path destination = sessionFolder.resolve("${scan.id}_${resource.folder}")
                    Files.createDirectory(destination)
                    Globals.antBuilder.unzip(src: zippedResource.toFile(), dest: destination.toFile())
                    Files.delete(zippedResource)
                }
                session.removeScan(scan) // don't want to serialize these
            }

            Globals.antBuilder.zip(basedir: sessionFolder, destfile: sessionZip)
            sessionFolder.deleteDir()
        }

        customScans.each { scan ->
            scan.scanResources.each { resource ->
                parseAndDownloadResourceFiles(resource)
            }
        }

        session.assessors.each { assessor ->
            XMLUtils.extractExperimentXmlFromXnat("data/projects/${project}/subjects/${subject}/experiments/${session}/assessors/${assessor.label}",
                    PathUtils.exportDir(project.id).resolve("${assessor}.xml").toFile())

            if (assessor.resources.isEmpty()) {
                assessor.extension(new SimpleSessionAssessorExportExtension())
            } else {
                assessor.extension(new DefaultSessionAssessorExportExtension())
                assessor.resources.each { resource ->
                    parseAndDownloadResourceFiles(resource)
                }
            }
        }
    }

}