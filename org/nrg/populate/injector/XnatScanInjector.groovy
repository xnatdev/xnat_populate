package org.nrg.populate.injector

import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan

interface XnatScanInjector extends XnatParserInjector {

    void injectData(Project project, Subject subject, ImagingSession session, Scan scan)

}
