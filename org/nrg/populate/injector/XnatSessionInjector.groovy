package org.nrg.populate.injector

import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession

interface XnatSessionInjector extends XnatParserInjector {

    void injectData(Project project, Subject subject, ImagingSession session)

}
