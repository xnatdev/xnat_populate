package org.nrg.populate.injector.injectors

import org.nrg.populate.injector.XnatScanInjector
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan
import org.nrg.xnat.pogo.resources.ScanResource

class XnatScanResourceInjector implements XnatScanInjector, ArchetypalInjectorBase {

    @Override
    void injectData(Project project, Subject subject, ImagingSession session, Scan scan) {
        replacements.put('$group', subject.group)
        replacements.put('$sessionLabel', session.label)
        replacements.put('$scanId', scan.id)
        final ScanResource scanResource = read(ScanResource)
        scanResource.project(project).subject(subject).subjectAssessor(session).scan(scan)
        scan.addResource(scanResource)
    }

}
