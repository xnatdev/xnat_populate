package org.nrg.populate.injector.injectors

import org.nrg.populate.injector.XnatProjectInjector
import org.nrg.xnat.pogo.Project

class XPopDefaultUsersSuppressor implements XnatProjectInjector {

    @Override
    void injectData(Project project) {
        project.owners.removeIf { it.username == 'owner' }
        project.members.removeIf { it.username == 'member' }
        project.collaborators.removeIf { it.username == 'collaborator' }
    }

}
