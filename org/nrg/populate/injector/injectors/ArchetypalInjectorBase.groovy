package org.nrg.populate.injector.injectors

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.nrg.populate.injector.XnatParserInjector
import org.nrg.populate.jackson.deserializers.ToStringDeserializer
import org.nrg.testing.CommonStringUtils

trait ArchetypalInjectorBase implements XnatParserInjector {

    String archetype
    Map<String, String> replacements = [:]

    def <X> X read(Class<X> toClass) {
        objectMapper.readValue(CommonStringUtils.replaceRecursively(archetype, replacements), toClass)
    }

    @JsonDeserialize(using = ToStringDeserializer)
    void setArchetype(String archetype) {
        this.archetype = archetype
    }

}