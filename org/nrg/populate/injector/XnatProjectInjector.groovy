package org.nrg.populate.injector

import org.nrg.xnat.pogo.Project

interface XnatProjectInjector extends XnatParserInjector {

    void injectData(Project project)

}
