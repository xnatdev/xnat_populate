package org.nrg.populate

import com.jayway.restassured.RestAssured
import org.nrg.populate.enums.ActionRequired
import org.nrg.populate.tcia.TciaProcessor
import org.nrg.populate.tcia.TciaSpec
import org.nrg.populate.util.ConsoleUtils
import org.nrg.populate.util.Globals
import org.nrg.populate.util.PathUtils
import org.nrg.populate.util.YamlUtils
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.testing.HttpUtils
import org.nrg.testing.XnatDownloadServerClient
import org.nrg.xnat.pogo.Project

class DataManager {
    private final boolean refreshData
    private final boolean refreshYaml
    private final List<String> projects
    private final List<String> tciaIds
    private final XnatDownloadServerClient downloadClient = new XnatDownloadServerClient()

    DataManager(boolean refreshData, boolean refreshYaml, List<String> projectList, List<String> tciaList) {
        this.refreshData = refreshData
        this.refreshYaml = refreshYaml
        projects = projectList
        tciaIds = tciaList
    }

    void clean() {
        if (refreshData) {
            new File('data').deleteDir()
        }
        if (refreshYaml) {
            if (new File('data').exists()) {
                for (String project in projects) {
                    if (new File(PathUtils.defaultPathForYaml(project)).exists()) {
                        new File(PathUtils.defaultPathForYaml(project)).delete()
                    }
                }
            }
        }
    }

    void downloadData() {
        final List<Project> requiredData = []
        final List<Project> requireUnzip = []
        new File('data').mkdir()

        List<Project> projectList = YamlUtils.deserializeProjects(projects, this)
        if (tciaIds.size() > 0) {
            final TciaProcessor tciaProcessor = new TciaProcessor()
            final List<String> allCollections = tciaProcessor.readCollectionNames()
            tciaIds.each { tciaId ->
                final String matchedId = allCollections.find { possibleMatch ->
                    possibleMatch.equalsIgnoreCase(tciaId)
                }
                if (matchedId != null) {
                    final Project syntheticProject = new Project(matchedId.replace(' ', '_')).runningTitle(matchedId).title(matchedId).description("This project mirrors the corresponding collection from TCIA: https://wiki.cancerimagingarchive.net/display/Public/${matchedId}")
                    final XPopProjectSetupExtension projectExtension = new XPopProjectSetupExtension(syntheticProject)
                    projectExtension.setTciaSpec(new TciaSpec(collections : [matchedId]))
                    projectExtension.setSrc('empty')
                    Globals.projects << syntheticProject
                } else {
                    ConsoleUtils.inform("Could not find a TCIA collection with ID ${tciaId} in the list of known collections: ${allCollections}. This collection will be skipped.")
                }
            }
        }
        projectList.each { project ->
            switch (PathUtils.locateData(project)) {
                case ActionRequired.DOWNLOAD :
                    requiredData << project
                    break
                case ActionRequired.UNZIP :
                    requireUnzip << project
                    break
                default :
                    break
            }
        }

        if (requiredData.size() > 0) {
            requiredData.each { project ->
                final String projectId = project.id
                final XPopProjectSetupExtension extension = project.extension as XPopProjectSetupExtension
                final String remote = extension.remoteSource
                final String zipPath = PathUtils.defaultPathForZip(projectId)
                extension.setDataPath(zipPath)
                if (remote) {
                    ConsoleUtils.inform("Project ${projectId} required data from a remote URL: ${remote}. Downloading now...")
                    HttpUtils.saveBinaryResponseToFile(RestAssured.given().get(remote), new File(zipPath))
                } else {
                    println("Downloading data for ${projectId}...")
                    downloadClient.downloadToFile("xnat_populate/${projectId}.zip", new File(zipPath))
                }
            }
        }
        if (requireUnzip.size() + requiredData.size() > 0) {
            (requiredData + requireUnzip).each { project ->
                final List<String> currentDataDirectories = listDirectories()
                Globals.antBuilder.unzip(src: (project.extension as XPopProjectSetupExtension).dataPath, dest: 'data')
                (project.extension as XPopProjectSetupExtension).setDataPath(PathUtils.defaultPathForData(project.id))
                final String newDirectory = listDirectories().find { !currentDataDirectories.contains(it) }
                if (newDirectory != project.id) {
                    new File("data/${newDirectory}").renameTo("data/${project.id}")
                }
            }
        }
    }

    void downloadYaml(String projectId) {
        downloadClient.downloadToFile("xnat_populate/${projectId}_metadata.yaml", new File(PathUtils.defaultPathForYaml(projectId)))
    }

    List<String> listDirectories() {
        (new File('data').listFiles() as List).findResults { file ->
            file.isDirectory() ? file.name : null
        }
    }

}
