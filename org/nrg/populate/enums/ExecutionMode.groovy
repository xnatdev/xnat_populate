package org.nrg.populate.enums

enum ExecutionMode {
    CREATE, ADD, EXPORT // ADD currently unused
}