package org.nrg.populate.util

import org.nrg.populate.DataManager
import org.nrg.populate.xnat_objects.Pipeline
import org.nrg.xnat.pogo.Project

class YamlUtils {
    private static final List<Project> projectList = []

    static List<Project> deserializeProjects(List<String> projects, DataManager dataManager) {
        projectList.clear()
        projects.each { project ->
            final String yamlPath = PathUtils.findYaml(project, dataManager)
            if (yamlPath != null) {
                projectList << deserializeYaml(yamlPath, Project)
                println("Parsed YAML for project ${project}.")
            }
        }
        Globals.projects = projectList
        projectList
    }

    static Pipeline deserializePipeline(String pipeline) {
        new Pipeline(pipeline, deserializeYaml("pipelines/${pipeline}.yaml", Map))
    }

    static List<Project> getProjects() {
        projectList
    }

    static <X> X deserializeYaml(String yamlPath, Class<X> deserializedObjectClass) {
        Globals.XPOP_YAML_READER.readValue(new File(yamlPath), deserializedObjectClass)
    }

    static <X> X deserializeString(String yaml, Class<X> deserializedObjectClass) {
        Globals.XPOP_YAML_READER.readValue(yaml, deserializedObjectClass)
    }

    static void writeToFile(File file, Object object) {
        Globals.XPOP_YAML_WRITER.writeValue(file, object)
    }

    static String formDataToYaml(String formData) {
        String yaml = ''
        String[] pairs = formData.split('&')
        for (String pair in pairs) {
            String[] splitPair = pair.split('=')
            String parameter = URLDecoder.decode(splitPair[0], 'UTF-8').replace("'", "\\'")
            String value = (splitPair.length > 1) ? URLDecoder.decode(pair.split('=')[1], 'UTF-8').replace("'", "\\'") : ''
            yaml += "'${parameter}' : '${value}'\n"
        }
        yaml
    }

}
