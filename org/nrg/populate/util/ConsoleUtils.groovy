package org.nrg.populate.util

class ConsoleUtils {

    private static final String ANSI_ESCAPE = "\u001B["
    private static final String RESET = "\u001B[0m"

    static void warn(String message) {
        println("${ANSI_ESCAPE}1;40;38;5;9m${message}") // 1 - bold, 40 - background black, 38;5;9 - foreground color (38;5) of 9 (red)
        reset()
    }

    static void inform(String message) {
        println("${ANSI_ESCAPE}40;38;5;11m${message}") // 40 - background black, 38;5;11 - foreground color (38;5) of 11 (yellow)
        reset()
    }

    static void reset() {
        print(RESET)
    }
    
}
