package org.nrg.populate.util

import com.fasterxml.jackson.databind.ObjectMapper
import org.nrg.populate.enums.ExecutionMode
import org.nrg.populate.jackson.object_mapping.YamlReader
import org.nrg.populate.jackson.object_mapping.YamlWriter
import org.nrg.xnat.interfaces.XnatInterface
import org.nrg.xnat.pogo.Project

class Globals {

    public static String siteUrl
    public static XnatInterface xnatClient
    public static List<Project> projects
    public static ExecutionMode mode = ExecutionMode.CREATE
    public static int delay
    public static final AntBuilder antBuilder = new AntBuilder()
    public static final String XPOP_METADATA_RESOURCE = 'about'
    public static final ObjectMapper XPOP_YAML_READER = new YamlReader()
    public static final ObjectMapper XPOP_YAML_WRITER = new YamlWriter()

    static void sleep() {
        if (delay > 0) {
            ConsoleUtils.inform("${delay} millisecond delay...")
            sleep(delay)
        }
    }

}
