package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

import org.nrg.populate.tcia.TciaSpec
import org.nrg.populate.util.ConsoleUtils
import org.nrg.populate.util.Globals
import org.nrg.populate.xnat_objects.extensions.DefaultSubjectExtension
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.populate.injector.XnatParserInjector
import org.nrg.populate.xnat_objects.extensions.XPopRootResourceFileExtension
import org.nrg.xnat.jackson.deserializers.ProjectDeserializer
import org.nrg.xnat.pogo.AnonScript
import org.nrg.xnat.pogo.Investigator
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.custom_variable.CustomVariableSet
import org.nrg.xnat.pogo.events.Subscription
import org.nrg.xnat.pogo.resources.ProjectResource
import org.nrg.xnat.pogo.resources.Resource
import org.nrg.xnat.pogo.resources.ResourceFile
import org.nrg.xnat.pogo.users.CustomUserGroup
import org.nrg.xnat.pogo.users.User
import org.nrg.xnat.pogo.users.UserGroup
import org.nrg.xnat.pogo.users.UserGroups

import java.time.LocalDate

@SuppressWarnings("GrMethodMayBeStatic")
class XPopProjectDeserializer extends ProjectDeserializer {

    @Override
    Project deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final Project project = super.deserialize(codec, node)
        final XPopProjectSetupExtension extension = new XPopProjectSetupExtension(project)

        setStringIfNonnull(node, 'id', project.&setId)
        setStringIfNonnull(node, 'runningTitle', project.&setRunningTitle)
        setStringIfNonnull(node, 'title', project.&setTitle)

        setSimpleStringList(node, 'aliases', project.&setAliases)
        setStringIfNonnull(node, 'accessibility', project.&setAccessibility)

        setObject(node, 'pi', codec, Investigator, project.&setPi)
        setObjectList(node, 'investigators', codec, Investigator, project.&setInvestigators)

        setMapList(node, 'customUserGroups', codec, CustomUserGroup, 'name', project.&setCustomUserGroups)

        if (fieldNonnull(node, 'users')) {
            final JsonNode users = node.get('users')

            users.fields().each { entry ->
                final String groupString = entry.key
                final Map<String, UserGroup> defaultGroups = ['owners' : UserGroups.OWNER, 'members' : UserGroups.MEMBER, 'collaborators' : UserGroups.COLLABORATOR]
                final UserGroup group = groupString in defaultGroups.keySet() ? defaultGroups.get(groupString) : project.customUserGroups.find { it.name == groupString }

                project.users.put(group, wrapUsernames(entry.value))
            }
        }

        if (!project.owners.any { it.username == 'owner' }) project.addOwner(new User('owner'))
        if (!project.members.any { it.username == 'member' }) project.addMember(new User('member'))
        if (!project.collaborators.any { it.username == 'collaborator' }) project.addCollaborator(new User('collaborator'))

        setResources(node, codec, ProjectResource, project.&setProjectResources)
        if (!project.projectResources.any { it.folder == Globals.XPOP_METADATA_RESOURCE }) {
            final Resource xpopMetadata = new ProjectResource(project, Globals.XPOP_METADATA_RESOURCE)
            for (String metadata in ['xnat_populate_cheat_sheet.md', 'xnat_populate_quick_summary.md']) {
                final ResourceFile resourceFile = new ResourceFile(xpopMetadata, metadata)
                resourceFile.extension(new XPopRootResourceFileExtension(resourceFile))
                xpopMetadata.addResourceFile(resourceFile)
            }
            project.addResource(xpopMetadata)
        }
        setMapList(node, 'subjects', codec, Subject, 'label', project.&setSubjects)
        setMapList(node, 'secondarySubjects', codec, Subject, 'label', project.&setSecondarySubjects)
        if (fieldNonnull(node, 'simpleSubjects')) {
            node.get('simpleSubjects').each { label ->
                final Subject subject = new Subject(project, label.asText())
                subject.extension(new DefaultSubjectExtension(subject))
            }
        }

        setObject(node, 'anon', codec, AnonScript, extension.&setAnonScript)
        setStringIfNonnull(node, 'src', extension.&setSrc)
        setStringIfNonnull(node, 'remoteSource', extension.&setRemoteSource)
        if (fieldNonnull(node, 'pipelines')) extension.setPipelineMaps((codec as ObjectMapper).readValue(node.get('pipelines').toString(), NESTED_STRING_MAP) as Map<String, Map<String, String>>)
        setSimpleStringList(node, 'pluginDependencies', extension.&setPluginDependencies)
        setSimpleStringList(node, 'enableWrappers', extension.&setRequiredCommandWrappers)
        if (fieldNonnull(node, 'lastUpdated')) {
            extension.setLastUpdated(LocalDate.parse(node.get('lastUpdated').asText()))
        }
        setSimpleStringList(node, 'projectDependencies', extension.&setProjectDependencies)
        setObjectList(node, 'subscriptions', codec, Subscription, project.&setSubscriptions)
        setObjectList(node, 'variableSets', codec, CustomVariableSet, project.&setVariableSets)

        setObjectList(node, 'injectors', codec, XnatParserInjector, extension.&setInjectors)
        extension.injectors.each { injector ->
            injector.setObjectMapper(codec as ObjectMapper)
        }

        if (fieldNonnull(node, 'parallelization')) {
            node.get('parallelization').each {
                switch (it.asText()) {
                    case 'subjects':
                        project.enableSubjectParallelization()
                        break
                    case 'subject_assessors':
                        project.enableSubjectAssessorParallelization()
                        break
                    case 'session_assessors':
                        project.enableSessionAssessorParallelization()
                        break
                    case 'scans':
                        project.enableScanParallelization()
                        break
                    default:
                        ConsoleUtils.warn("Unknown parallelization option: ${it}")
                }
            }
        }

        setObject(node, 'tciaSpec', codec, TciaSpec, extension.&setTciaSpec)

        project
    }

    private List<User> wrapUsernames(JsonNode userArrayNode) {
        userArrayNode.collect { user ->
            new User(user.asText())
        }
    }

}
