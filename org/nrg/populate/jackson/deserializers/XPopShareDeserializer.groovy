package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.Share

class XPopShareDeserializer extends CustomDeserializer {

    public static final XPopShareDeserializer INSTANCE = new XPopShareDeserializer()

    @Override
    Object deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        throw new UnsupportedOperationException('XPopShareDeserializer should not be used for direct deserialization.')
    }

    void setShares(JsonNode parentNode, Closure setShareMethod) {
        if (fieldNonnull(parentNode, 'share')) {
            setShareMethod(parentNode.get('share').fields().collect { entry ->
                new Share().destinationProject(entry.key).destinationLabel(entry.value.asText())
            })
        }
    }

}
