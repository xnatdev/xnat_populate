package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSessionAssessorExtension
import org.nrg.populate.xnat_objects.extensions.experiments.QueryBasedSessionAssessorExtension
import org.nrg.xnat.pogo.experiments.SessionAssessor
import org.nrg.xnat.pogo.extensions.session_assessor.SessionAssessorExtension
import org.nrg.xnat.pogo.resources.SessionAssessorResource

class XPopSessionAssessorDeserializer extends XPopExperimentDeserializer {

    @Override
    SessionAssessor deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final SessionAssessor assessor = new SessionAssessor()

        final SessionAssessorExtension extension = (fieldNonnull(node, 'src') && node.get('src').asText() == 'empty') ? new QueryBasedSessionAssessorExtension(assessor) : new DefaultSessionAssessorExtension(assessor)
        setCommonFields(node, assessor, codec, SessionAssessorResource)

        assessor
    }

}
