package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import org.nrg.populate.xnat_objects.ReportAction
import org.nrg.populate.xnat_objects.SiteConfiguration
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.AnonScript
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.containers.Image

class XPopSiteConfigDeserializer extends CustomDeserializer<SiteConfiguration> {

    @Override
    SiteConfiguration deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final SiteConfiguration configuration = new SiteConfiguration()

        setObject(node, 'pipelines', codec, Map, configuration.&setPipelineMaps)
        setObject(node, 'anon', codec, AnonScript, configuration.&setAnonScript)
        setObject(node, 'siteConfig', codec, Map, configuration.&setSiteConfig)
        if (fieldNonnull(node, 'dataTypes')) {
            final JsonNode typesField = node.get('dataTypes')
            if (fieldNonnull(typesField, 'sessions')) {
                typesField.get('sessions').collect { textNode ->
                    configuration.dataTypes << DataType.makeSessionDataType(textNode.asText())
                }
                (typesField as ObjectNode).remove('sessions')
            }
            configuration.dataTypes.addAll(readMapObjectList(typesField, codec, DataType, 'code'))
        }
        setObjectList(node, 'reportActions', codec, ReportAction, configuration.&setReportActions)
        setSimpleStringList(node, 'searches', configuration.&setSearches)

        configuration
    }

}
