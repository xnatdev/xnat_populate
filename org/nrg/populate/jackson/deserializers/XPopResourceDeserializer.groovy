package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.extensions.ResourcePathExtension
import org.nrg.populate.xnat_objects.extensions.XPopResourceFileExtension
import org.nrg.xnat.jackson.deserializers.ResourceDeserializer
import org.nrg.xnat.pogo.Extension
import org.nrg.xnat.pogo.extensions.ResourceFileExtension
import org.nrg.xnat.pogo.resources.Resource
import org.nrg.xnat.pogo.resources.ResourceFile

class XPopResourceDeserializer<T extends Resource> extends ResourceDeserializer<T> {

    XPopResourceDeserializer(Class<? extends Resource> resourceClass) {
        super(resourceClass)
    }

    @Override
    Resource deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final Resource resource = super.deserialize(codec, node)
        final Extension<Resource> extension = new ResourcePathExtension(resource)
        final List<ResourceFile> files = []

        if (fieldNonnull(node, 'files')) {
            files.addAll(node.get('files').collect { file ->
                final ResourceFile resourceFile = new ResourceFile().name(file.asText())
                final ResourceFileExtension resourceFileExtension = new XPopResourceFileExtension(resourceFile)
                resourceFile
            })
        }

        if (fieldNonnull(node, 'complexFiles')) {
            files.addAll(readMapObjectList(node.get('complexFiles'), codec, ResourceFile, 'name'))
        }

        resource.resourceFiles(
                files.collect { file ->
                    file.resourceFolder(resource)
                }
        )

        setStringIfNonnull(node, 'subFolder', extension.&setSubFolder)

        resource
    }

}
