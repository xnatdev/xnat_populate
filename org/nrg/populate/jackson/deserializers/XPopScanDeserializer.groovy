package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.extensions.XPopScanSetupExtension
import org.nrg.xnat.jackson.deserializers.ScanDeserializer
import org.nrg.xnat.pogo.experiments.Scan
import org.nrg.xnat.pogo.resources.ScanResource

class XPopScanDeserializer extends ScanDeserializer {

    @Override
    Scan deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final Scan scan = super.deserialize(codec, node)

        setStringIfNonnull(node, 'seriesDescription', scan.&setSeriesDescription)
        setResources(node, codec, ScanResource, scan.&setScanResources)
        if (fieldNonnull(node, 'containers')) {
            setObject(node, 'containers', codec, Map, new XPopScanSetupExtension(scan).&setContainers)
        }

        scan
    }

}
