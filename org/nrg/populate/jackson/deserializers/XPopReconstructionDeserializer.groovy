package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.extensions.XPopReconstructionSetupExtension
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.Reconstruction
import org.nrg.xnat.pogo.resources.ReconstructionResource

class XPopReconstructionDeserializer extends CustomDeserializer<Reconstruction> {

    @Override
    Reconstruction deserialize(ObjectCodec codec, JsonNode reconNode) throws IOException, JsonProcessingException {
        final Reconstruction reconstruction = new Reconstruction()

        setStringIfNonnull(reconNode, 'label', reconstruction.&setLabel)
        setStringIfNonnull(reconNode, 'type', reconstruction.&setType)
        setStringIfNonnull(reconNode, 'baseScanType', reconstruction.&baseScanType)
        setResources(reconNode, codec, ReconstructionResource, reconstruction.&setResources)
        // noinspection GroovyResultOfObjectAllocationIgnored
        new XPopReconstructionSetupExtension(reconstruction)

        reconstruction
    }

}
