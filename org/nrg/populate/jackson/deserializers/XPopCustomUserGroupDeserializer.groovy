package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.xnat.enums.DataAccessLevel
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.users.CustomUserGroup

class XPopCustomUserGroupDeserializer extends CustomDeserializer<CustomUserGroup> {

    @Override
    CustomUserGroup deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final CustomUserGroup group = new CustomUserGroup()

        setStringIfNonempty(node, 'name', group.&setName)

        node.fields().each { dataEntry ->
            if (dataEntry.key != 'name') group.accessLevelMap.put(DataType.lookup(dataEntry.key), readEnumByName(dataEntry.value.asText(), DataAccessLevel))
        }

        group
    }

}
