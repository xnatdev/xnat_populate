package org.nrg.populate.jackson.object_mapping

import com.fasterxml.jackson.databind.DeserializationFeature
import org.nrg.populate.jackson.modules.XPopYamlDeserializationModule

class YamlReader extends XPopYamlMapper {

    YamlReader() {
        super()
        registerModule(new XPopYamlDeserializationModule())
        enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING)
    }

}
