package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.apache.commons.lang3.StringUtils
import org.nrg.populate.xnat_objects.extensions.ResourcePathExtension
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.pogo.resources.Resource
import org.nrg.xnat.pogo.resources.ResourceFile

@SuppressWarnings("GroovyAssignabilityCheck")
class XPopExportResourceSerializer extends CustomSerializer<Resource> {

    @Override
    void serialize(Resource value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObjectFieldStart(value.folder)

        final List<ResourceFile> complexFiles, simpleFiles
        (complexFiles, simpleFiles) = value.resourceFiles.split { (it.name.contains('/')) || it.unzip || StringUtils.isNotEmpty(it.content) }

        if (!simpleFiles.isEmpty()) {
            gen.writeObjectField('files', simpleFiles.collect { it.name })
        }

        writeObjectListAsMap(gen, 'complexFiles', complexFiles)
        writeStringFieldIfNonempty(gen, 'subFolder', (value.extension as ResourcePathExtension).subFolder)

        gen.writeEndObject()
    }
}
