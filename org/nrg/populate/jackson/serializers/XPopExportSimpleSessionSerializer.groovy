package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.pogo.experiments.ImagingSession

class XPopExportSimpleSessionSerializer extends CustomSerializer<ImagingSession> {

    static final XPopExportSimpleSessionSerializer INSTANCE = new XPopExportSimpleSessionSerializer()

    @Override
    void serialize(ImagingSession value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(value.label)
    }

}