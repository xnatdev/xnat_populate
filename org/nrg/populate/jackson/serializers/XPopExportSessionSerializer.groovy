package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.populate.xnat_objects.extensions.experiments.export.SessionAssessorExportExtension
import org.nrg.populate.xnat_objects.extensions.experiments.export.SimpleSessionAssessorExportExtension
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.SessionAssessor

@SuppressWarnings("GroovyAssignabilityCheck")
class XPopExportSessionSerializer extends CustomSerializer<ImagingSession> {

    static final XPopExportSessionSerializer INSTANCE = new XPopExportSessionSerializer()

    @Override
    void serialize(ImagingSession value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObjectFieldStart(value.label)
        writeXnatResources(gen, value.resources)
        writeObjectListAsMap(gen, 'scans', value.scans)
        writeAssessors(gen, value)
        injectAdditionalFields(gen, value)
        gen.writeEndObject()
    }

    private writeAssessors(JsonGenerator gen, ImagingSession session) {
        final List<SessionAssessor> simple, complex
        (simple, complex) = session.assessors.split { it.extension instanceof SimpleSessionAssessorExportExtension }
        writeListFieldIfNonempty(gen, 'assessors', simple.collect { it.label })
        serializeAssessorList(gen, 'complexAssessors', complex)
    }

    protected void injectAdditionalFields(JsonGenerator gen, ImagingSession session) {}

    protected serializeAssessorList(JsonGenerator gen, String fieldName, List<SessionAssessor> experiments) {
        if (!experiments.isEmpty()) {
            gen.writeObjectFieldStart(fieldName)
            experiments.each { experiment ->
                (experiment.extension as SessionAssessorExportExtension).serializer().serialize(experiment, gen, null)
            }
            gen.writeEndObject()
        }
    }

}
