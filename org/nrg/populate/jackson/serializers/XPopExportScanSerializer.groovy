package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.ScanSerializer
import org.nrg.xnat.pogo.experiments.Scan

class XPopExportScanSerializer extends ScanSerializer {

    @SuppressWarnings("GroovyAssignabilityCheck")
    @Override
    void serialize(Scan value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObjectFieldStart(value.id)
        writeStringFieldIfNonnull(gen, 'seriesDescription', value.seriesDescription)
        writeCommonFields(gen, value)
        writeXnatResources(gen, value.scanResources)
        gen.writeEndObject()
    }

}
