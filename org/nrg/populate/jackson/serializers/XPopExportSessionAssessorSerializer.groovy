package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.pogo.experiments.SessionAssessor

class XPopExportSessionAssessorSerializer extends CustomSerializer<SessionAssessor> {

    static final XPopExportSessionAssessorSerializer INSTANCE = new XPopExportSessionAssessorSerializer()

    @Override
    void serialize(SessionAssessor value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObjectFieldStart(value.label)
        //noinspection GroovyAssignabilityCheck
        writeXnatResources(gen, value.resources)
        if (!value.specificFields.isEmpty()) {
            gen.writeObjectField('fields', value.specificFields)
        }
        gen.writeEndObject()
    }

}
