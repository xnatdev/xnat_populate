package org.nrg.populate.tcia

import com.fasterxml.jackson.annotation.JsonProperty

class TciaSeries implements TciaFilterable {

    @JsonProperty('SeriesInstanceUID') String seriesInstanceUID
    @JsonProperty('StudyInstanceUID') String studyInstanceUID
    @JsonProperty('Modality') String modality
    @JsonProperty('ProtocolName') String protocolName
    @JsonProperty('SeriesDate') String seriesDate
    @JsonProperty('SeriesDescription') String seriesDescription
    @JsonProperty('BodyPartExamined') String bodyPartExamined
    @JsonProperty('SeriesNumber') String seriesNumber // badly formatted from TCIA
    @JsonProperty('Collection') String collection
    @JsonProperty('Manufacturer') String manufacturer
    @JsonProperty('ManufacturerModelName') String manufacturerModelName
    @JsonProperty('SoftwareVersions') String softwareVersions
    @JsonProperty('Visibility') String visibility
    @JsonProperty('ImageCount') int imageCount
    @JsonProperty('AnnotationsFlag') String annotationsFlag

}
