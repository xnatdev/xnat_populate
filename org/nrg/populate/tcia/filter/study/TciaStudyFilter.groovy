package org.nrg.populate.tcia.filter.study

import org.nrg.populate.tcia.TciaStudy
import org.nrg.populate.tcia.filter.TciaDataFilter

abstract class TciaStudyFilter extends TciaDataFilter<TciaStudy> {}
