package org.nrg.populate.tcia.filter.study

import org.nrg.populate.tcia.TciaStudy

class PatientSexFilter extends TciaStudyFilter {

    String includedSex

    @Override
    void filter(List<TciaStudy> studies) {
        studies.removeIf { study ->
            study.patientSex != includedSex
        }
    }

}
