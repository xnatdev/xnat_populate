package org.nrg.populate.tcia.filter.study

import org.nrg.populate.tcia.TciaStudy

class StudiesWithModality extends TciaStudyFilter {

    List<String> modalities = []

    @Override
    void filter(List<TciaStudy> filterables) {
        filterables.removeIf { study ->
            !study.series.any { it.modality in modalities }
        }
    }

}
