package org.nrg.populate.tcia.filter.study

import org.nrg.populate.tcia.TciaStudy

class PatientIDFilter extends TciaStudyFilter {

    List<String> allowedPatients = []
    boolean regex = false

    @Override
    void filter(List<TciaStudy> studies) {
        studies.removeIf { study ->
            regex ? !allowedPatients.any { study.patientID.matches(it) } : !(study.patientID in allowedPatients)
        }
    }

}
