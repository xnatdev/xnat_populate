package org.nrg.populate.tcia.filter.series

import org.nrg.populate.tcia.TciaSeries
import org.nrg.populate.tcia.filter.TciaDataFilter

abstract class TciaSeriesFilter extends TciaDataFilter<TciaSeries> {}
