package org.nrg.populate.tcia

import org.nrg.populate.tcia.filter.TciaDataFilter

class TciaSpec {

    List<String> collections = []
    String subjectNaming = '$patientName'
    String sessionNaming = '$patientName_$hashStudyInstanceUID'
    List<TciaDataFilter> filters = []

}
